package framework.behavior;

import framework.RobotPlayer;
import framework.behavior.Movement.DirectionValidator;
import framework.messaging.ChannelType;
import framework.messaging.SharedVisionSystem;
import framework.messaging.SharedVisionSystem.EnemyData;
import framework.navigation.NavigationSystem;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;

public class Swarming {
	private static RobotSwarm mySwarm;
	private static MapLocation leaderLocation;
	private static SwarmOrder order;

	private static Direction nextMove;

	public static void init() {
		int lobbyValue = ChannelType.SWARM_LOBBY.read(0);
		mySwarm = RobotSwarm.values()[lobbyValue % 3];
		ChannelType.SWARM_LOBBY.write(0, lobbyValue + 1);
		leaderLocation = null;
		order = null;
		nextMove = null;
	}

	public static void calculate() {
		order = mySwarm.getOrder();
		MapLocation myLocation = RobotPlayer.rc.getLocation();
		if (isLeader()) {
			leaderLocation = myLocation;
			if (nextMove == null) {
				nextMove = NavigationSystem.navigate(order.location);
			}
		} else {
			leaderLocation = mySwarm.getLeaderLocation();
			if (Combat.enemiesInRange(myLocation, 35).length != 0) {
				if (myLocation.distanceSquaredTo(leaderLocation) > 50) {
					nextMove = myLocation.directionTo(leaderLocation);
				} else {
					nextMove = myLocation.directionTo(order.location);
				}
			} else {
				if (RobotPlayer.rc.getLocation().distanceSquaredTo(
						leaderLocation) > 100) {
					nextMove = NavigationSystem.navigate(order.location);
				} else {
					nextMove = myLocation.directionTo(leaderLocation);
				}
			}
		}
	}

	public static void perform() {
		MapLocation myLocation = RobotPlayer.rc.getLocation();
		if (Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 35).length > 0) {
			MapLocation target = order.location;
			if (RobotPlayer.rc.getLocation().distanceSquaredTo(target) <= 100) {
				Combat.killEnemies(
						RobotPlayer.rc.getLocation().directionTo(target),
						order.type == OrderType.ATTACK);
			} else {
				Combat.killEnemies(nextMove, order.type == OrderType.ATTACK);
			}
			return;
		}
		for (EnemyData data : SharedVisionSystem.globalData) {
			if (myLocation.distanceSquaredTo(data.location) <= 100) {
				Direction toEnemy = myLocation.directionTo(data.location);
				if (Math.abs(toEnemy.ordinal() - nextMove.ordinal()) <= 1) {
					Movement.tryToMove(myLocation.directionTo(data.location),
							false);
				}
				return;
			} else if (myLocation.distanceSquaredTo(data.location) <= 75) {
				if (Combat.alliesInRange(data.location, 10).length > 0) {
					Movement.tryToMove(myLocation.directionTo(data.location),
							false);
				}
				return;
			}
		}
		if (isLeader()) {
			Movement.tryToMove(
					nextMove,
					(order.type == OrderType.DEFEND && RobotPlayer.rc
							.getLocation().distanceSquaredTo(order.location) <= 100));
		} else {
			Movement.tryToMove(
					nextMove,
					(order.type == OrderType.DEFEND && RobotPlayer.rc
							.getLocation().distanceSquaredTo(order.location) <= 100),
					new LeaderSeparation());
		}
		nextMove = null;
	}

	public static boolean isLeader() {
		int leaderID = mySwarm.getLeaderID();
		if (leaderID == 0) {
			mySwarm.takeLeadership();
			return true;
		}
		return leaderID == RobotPlayer.rc.getRobot().getID();
	}

	public static enum RobotSwarm {
		SWARM_1, SWARM_2, SWARM_3;

		public int getLeaderID() {
			return ChannelType.SWARM_LEADER.read(2 * ordinal());
		}

		public void setLeaderID(int id) {
			ChannelType.SWARM_LEADER.write(2 * ordinal(), id);
		}

		public MapLocation getLeaderLocation() {
			int message = ChannelType.SWARM_LEADER.read(2 * ordinal() + 1);
			byte x = (byte) ((message >> 8) & 0x000000FF);
			byte y = (byte) (message & 0x000000FF);
			return new MapLocation(x, y);
		}

		public void takeLeadership() {
			ChannelType.SWARM_LEADER.write(2 * ordinal(), RobotPlayer.rc
					.getRobot().getID());
			writeLeaderLocation(RobotPlayer.rc.getLocation());
		}

		public void writeLeaderLocation(MapLocation location) {
			int message = (((location.x & 0x000000FF) << 8) | (location.y & 0x000000FF));
			ChannelType.SWARM_LEADER.write(2 * ordinal() + 1, message);
		}

		public SwarmOrder getOrder() {
			return SwarmOrder.fromMessage(ChannelType.SWARM_ORDERS
					.read(ordinal()));
		}

		public void giveOrder(SwarmOrder order) {
			ChannelType.SWARM_ORDERS.write(ordinal(), order.toMessage());
		}
	}

	public static class LeaderSeparation implements DirectionValidator {
		@Override
		public boolean isValid(Direction direction) {
			return (RobotPlayer.rc.getLocation().add(direction)
					.distanceSquaredTo(leaderLocation) > 8);
		}
	}

	public static class SwarmOrder {
		public OrderType type;
		public MapLocation location;

		public SwarmOrder(OrderType type, MapLocation location) {
			this.type = type;
			this.location = location;
		}

		public static SwarmOrder fromMessage(int message) {
			byte type = (byte) ((message >> 16) & 0x000000FF);
			byte x = (byte) ((message >> 8) & 0x000000FF);
			byte y = (byte) (message & 0x000000FF);
			return new SwarmOrder(OrderType.values()[type], new MapLocation(x,
					y));
		}

		public int toMessage() {
			return (((type.ordinal() & 0x000000FF) << 16)
					| ((location.x & 0x000000FF) << 8) | (location.y & 0x000000FF));
		}
	}

	public static enum OrderType {
		DEFEND, ATTACK;
	}

	public static void findSwarmLeaders() {
		findSwarmLeader(RobotSwarm.SWARM_1);
		findSwarmLeader(RobotSwarm.SWARM_2);
		findSwarmLeader(RobotSwarm.SWARM_3);
	}

	private static void findSwarmLeader(RobotSwarm swarm) {
		int leaderID = swarm.getLeaderID();
		if (leaderID == 0)
			return;
		MapLocation leaderLoc = swarm.getLeaderLocation();
		for (Robot robot : RobotPlayer.rc.senseNearbyGameObjects(Robot.class,
				leaderLoc, 2, RobotPlayer.team)) {
			if (robot.getID() == leaderID) {
				try {
					swarm.writeLeaderLocation(RobotPlayer.rc
							.senseRobotInfo(robot).location);
					return;
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
		swarm.setLeaderID(0);
	}
}