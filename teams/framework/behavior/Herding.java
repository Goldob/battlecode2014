package framework.behavior;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import framework.RobotPlayer;



import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotLevel;

public class Herding {
	// At the moment it only works for tower herding

	private static int workingRangeSq;
	private static Queue<HerdingData> plannedHerding;

	public static void init() {
		Herding.workingRangeSq = RobotPlayer.rc.getType().attackRadiusMaxSquared;
		plannedHerding = new LinkedList<HerdingData>();
	}

	public static void autoPrepareTargets() {
		clearTargets();
		MapLocation location = RobotPlayer.rc.getLocation();
		for (MapLocation pastrLoc : RobotPlayer.rc
				.sensePastrLocations(RobotPlayer.rc.getTeam())) {
			if (location.distanceSquaredTo(pastrLoc) <= workingRangeSq) {
				addTarget(pastrLoc);
			}
		}
		if (plannedHerding.isEmpty()) {
			addTarget(RobotPlayer.rc.getLocation());
		}
	}

	public static void addTarget(MapLocation location) {
		plannedHerding.add(HerdingData.calculate(location));
	}

	public static void clearTargets() {
		plannedHerding.clear();
	}

	public static boolean isWorking() {
		return !plannedHerding.isEmpty();
	}

	public static void perform() {
		while (isWorking() && plannedHerding.peek().performHerding()) {
			plannedHerding.remove();
		}
	}

	private static class HerdingData {
		private Stack<MapLocation> locationsToShoot;

		private HerdingData(Stack<MapLocation> locationsToShoot) {
			this.locationsToShoot = locationsToShoot;
		}

		public static HerdingData calculate(MapLocation pastrLoc) {
			Stack<MapLocation> temp = new Stack<MapLocation>();
			for (Direction direction : Direction.values()) {
				if (direction != Direction.NONE && direction != Direction.OMNI) {
					MapLocation locToCheck = pastrLoc.add(direction);
					while (true) {
						locToCheck = locToCheck.add(direction);
						if (RobotPlayer.rc.senseTerrainTile(locToCheck)
								.isTraversableAtHeight(RobotLevel.ON_GROUND)
								&& RobotPlayer.rc.getLocation()
										.distanceSquaredTo(
												locToCheck.add(direction)) <= workingRangeSq) {
							temp.add(locToCheck.add(direction));
						} else {
							break;
						}
					}
				} else {
					break;
				}
			}
			return new HerdingData(temp);
		}

		/* Must return true if all the scheduled work is done */
		public boolean performHerding() {
			if (locationsToShoot.isEmpty()) {
				return true;
			}
			try {
				RobotPlayer.rc.attackSquareLight(locationsToShoot.pop());
			} catch (GameActionException e) {
				e.printStackTrace();
			}
			return false;
		}
	}
}
