package framework.control;

import java.util.LinkedList;
import java.util.Queue;

import framework.RobotPlayer;
import framework.behavior.Combat;
import framework.messaging.ChannelType;


import battlecode.common.Clock;
import battlecode.common.Robot;


public class TaskSystem {
	private static Task performedTask;
	private static int taskID;
	private static Queue<Task> scheduledTasks;

	private TaskSystem() {
	}

	public static void init() {
		performedTask = null;
		taskID = -1;
		scheduledTasks = new LinkedList<Task>();
	}

	public static boolean isPerformingTask() {
		if (performedTask != null) {
			if (performedTask.isExpired() && !RobotPlayer.rc.isConstructing()) {
				performedTask = null;
				TaskSystem.setTaskState(taskID, TaskState.FAILURE);
				taskID = -1;
				return false;
			} else if (getTaskState(taskID) != TaskState.IN_PROGRESS) {
				performedTask = null;
				taskID = -1;
				if (RobotPlayer.rc.isConstructing()) {
					// Harmless suicide. We don't want to selfdestruct when
					// there could be allies nearby.
					taskID = 1 / 0;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	public static boolean findNewTask() {
		if (performedTask != null && isPerformingTask()) {
			return false;
		}
		for (int i = 5; --i >= 0;) {
			if (getTaskState(i) == TaskState.READY) {
				performedTask = getTask(i);
				taskID = i;
				setTaskState(i, TaskState.IN_PROGRESS);
				return true;
			}
		}
		return false;
	}

	public static void taskCalculations() {
		if (performedTask != null) {
			performedTask.calculate();
		}
	}

	public static void performTask() {
		if (performedTask != null) {
			if (Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 35).length > 0) {
				performedTask.combat();
			} else {
				performedTask.perform();
			}
		} else if (Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 35).length > 0) {
			Combat.killEnemies(
					RobotPlayer.rc.getLocation().directionTo(
							RobotPlayer.rc.senseHQLocation()), false);
		}
	}

	public static void reportTaskCompletion() {
		if (performedTask != null) {
			setTaskState(taskID, TaskState.SUCCESS);
			performedTask = null;
			taskID = -1;
		}
	}

	public static void scheduleTask(Task task) {
		scheduledTasks.add(task);
	}

	public static void update() {
		for (int i = 5; --i >= 0;) {
			TaskState state = getTaskState(i);
			if (state == TaskState.NOT_ASSIGNED || state == TaskState.SUCCESS) {
				if (!scheduledTasks.isEmpty()) {
					setTask(i, scheduledTasks.poll());
					setTaskState(i, TaskState.READY);
				}

			} else if (state == TaskState.IN_PROGRESS) {
				Task task = getTask(i);
				if (task.isExpired()) {
					setTaskState(i, TaskState.FAILURE);
				} else if (Clock.getRoundNum() % 25 == 5 * i) {
					int id = ChannelType.TASK_SYSTEM.read(i * 2 + 1);
					boolean robotFound = false;
					for (Robot robot : RobotPlayer.rc.senseNearbyGameObjects(
							Robot.class, Integer.MAX_VALUE, RobotPlayer.team)) {
						if (robot.getID() == id) {
							robotFound = true;
							break;
						}
					}
					if (!robotFound) {
						setTaskState(i, TaskState.FAILURE);
					}
				}
			} else if (state == TaskState.FAILURE) {
				Task task = getTask(i);
				if (!task.isExpired()) {
					scheduledTasks.add(task);
				}
				setTaskState(i, TaskState.NOT_ASSIGNED);
			}
		}
	}

	public static void cancelAllTasks() {
		scheduledTasks.clear();
		for (int i = 5; --i >= 0;) {
			setTaskState(i, TaskState.NOT_ASSIGNED);
		}
	}

	public static void setTask(int index, Task task) {
		ChannelType.TASK_SYSTEM.write(2 * index, task.toMessage());
	}

	public static void setTaskState(int index, TaskState state) {
		ChannelType.TASK_SYSTEM.write(2 * index + 1, state.getCode());
	}

	public static Task getTask(int index) {
		return Task.read(ChannelType.TASK_SYSTEM.read(2 * index));
	}

	public static TaskState getTaskState(int index) {
		int message = ChannelType.TASK_SYSTEM.read(index * 2 + 1);
		if (message == 0) {
			return TaskState.NOT_ASSIGNED;
		} else if (message == -1) {
			return TaskState.READY;
		} else if (message == -2) {
			return TaskState.SUCCESS;
		} else if (message == -3) {
			return TaskState.FAILURE;
		} else {
			return TaskState.IN_PROGRESS;
		}
	}

	public static enum TaskState {
		// Any code higher than 0 is robot's id
		NOT_ASSIGNED(0), READY(-1), SUCCESS(-2), FAILURE(-3), IN_PROGRESS {
			@Override
			public int getCode() {
				return RobotPlayer.rc.getRobot().getID();
			}
		};

		private int code = Integer.MIN_VALUE;

		TaskState() {}

		TaskState(int code) {
			this.code = code;
		}

		public int getCode() {
			return code;
		}
	}
}