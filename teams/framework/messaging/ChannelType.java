package framework.messaging;

import framework.RobotPlayer;
import battlecode.common.GameActionException;

public enum ChannelType {
	NAVIGATION_DATA(0, 0), 
	NAVIGATION_GRAPH_3x3NODES(1, 1296), 
	NAVIGATION_GRAPH_9x9NODES(1297, 1440),

	SWARM_LOBBY(1441, 1441),
	SWARM_LEADER(1442, 1447),
	SWARM_ORDERS(1448, 1450),

	VISION_SYSTEM_INPUT_INDEX(1451, 1451),
	VISION_SYSTEM_INPUT(1452, 2801),
	VISION_SYSTEM_OUTPUT_LENGTH(2802, 2802),
	VISION_SYSTEM_OUTPUT(2803, 2827),

	TASK_SYSTEM(2828, 2837),

	COW_GROWTH_3x3NODES(2838, 4133),
	TACTICAL_DATA_9x9NODES(4134, 4277),
	TOTAL_COW_GROWTH(4288, 4288);

	private final int FIRST_INDEX;
	private final int LAST_INDEX;

	ChannelType(final int FIRST_INDEX, final int LAST_INDEX) {
		this.FIRST_INDEX = FIRST_INDEX;
		this.LAST_INDEX = LAST_INDEX;
	}

	public void write(int index, int message) {
		index += FIRST_INDEX;
		if (index <= LAST_INDEX && index >= FIRST_INDEX) {
			try {
				RobotPlayer.rc.broadcast(index, message);
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
	}

	public int read(int index) {
		index += FIRST_INDEX;
		if (index <= LAST_INDEX && index >= FIRST_INDEX) {
			try {
				return RobotPlayer.rc.readBroadcast(index);
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
}