package framework.base;

import framework.messaging.SharedVisionSystem;
import battlecode.common.Clock;

public class PASTRBase {
	/* I think PASTR is supposed to do navigation calculations etc */

	public static void init() {}

	public static void nextRound() {
		if (Clock.getRoundNum() % 5 == 0)
			SharedVisionSystem.update();
	}
}
