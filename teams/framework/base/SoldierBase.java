package framework.base;

import framework.RobotPlayer;
import framework.behavior.Movement;
import framework.behavior.Swarming;
import framework.control.TaskSystem;
import framework.messaging.SharedVisionSystem;
import framework.navigation.MapProcessing;
import framework.navigation.NavigationSystem;
import battlecode.common.Clock;

public class SoldierBase {
	private static SoldierState currentState = SoldierState.NOT_INITIALIZED;

	public static void init() {
		NavigationSystem.init();
		if (!NavigationSystem.isEnabled()
				&& RobotPlayer.rc.senseRobotCount() == 1) {
			MapProcessing.perform();
		}
		Movement.init();
		Swarming.init();
		TaskSystem.init();

		currentState = SoldierState.SWARMING;
	}

	public static void nextRound() {
		if (RobotPlayer.rc.isConstructing()) {
			if (RobotPlayer.rc.getConstructingRounds() <= 1) {
				TaskSystem.reportTaskCompletion();
			}
		}
		if (Clock.getRoundNum() % 5 == 0)
			SharedVisionSystem.update();
		if (!Swarming.isLeader()) {
			if (TaskSystem.isPerformingTask() || TaskSystem.findNewTask()) {
				currentState = SoldierState.PERFORMING_TASK;
			} else {
				currentState = SoldierState.SWARMING;
			}
		}

		currentState.calculate();
		if (RobotPlayer.rc.isActive())
			currentState.takeControl();
	}

	private static enum SoldierState {
		NOT_INITIALIZED {
			@Override
			public void takeControl() {
			}

			@Override
			public void calculate() {
			}
		},
		SWARMING {
			@Override
			public void takeControl() {
				Swarming.perform();
			}

			@Override
			public void calculate() {
				Swarming.calculate();
			}
		},
		PERFORMING_TASK {
			@Override
			public void takeControl() {
				TaskSystem.performTask();
			}

			@Override
			public void calculate() {
				TaskSystem.taskCalculations();
			}
		};

		public abstract void takeControl();

		public abstract void calculate();
	}
}
