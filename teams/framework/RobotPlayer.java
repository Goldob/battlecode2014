package framework;

import framework.base.HQBase;
import framework.base.NoiseTowerBase;
import framework.base.PASTRBase;
import framework.base.SoldierBase;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class RobotPlayer {
	public static RobotController rc = null;
	public static Team team;

	public static void run(RobotController robotController) {
		rc = robotController;
		team = rc.getTeam();
		while (true) {
			RobotType type = rc.getType();
			if (type == RobotType.HQ) {
				try {
					HQBase.init();
				} catch (TooMuchSwagException e) {
					e.printStackTrace();
				}
				while (rc.getType() == type) {
					HQBase.nextRound();
					rc.yield();
				}
			}
			if (type == RobotType.NOISETOWER) {
				NoiseTowerBase.init();
				while (rc.getType() == type) {
					NoiseTowerBase.nextRound();
					rc.yield();
				}
			}
			if (type == RobotType.PASTR) {
				PASTRBase.init();
				while (rc.getType() == type) {
					PASTRBase.nextRound();
					rc.yield();
				}
			}
			if (type == RobotType.SOLDIER) {
				SoldierBase.init();
				while (rc.getType() == type) {
					SoldierBase.nextRound();
					rc.yield();
				}
			}
		}
	}
}
