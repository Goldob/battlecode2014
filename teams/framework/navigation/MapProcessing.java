package framework.navigation;

import java.util.LinkedList;
import java.util.Queue;

import framework.RobotPlayer;
import framework.control.Tactical9x9Node;
import framework.messaging.ChannelType;



import battlecode.common.Direction;
import battlecode.common.MapLocation;
import battlecode.common.RobotLevel;
import battlecode.common.TerrainTile;

public class MapProcessing {
	private static boolean[][] bitmap;
	private static int width;
	private static int height;

	private static NavigationNode[][] graph3x3Nodes;
	private static NavigationNode[][] graph9x9Nodes;

	private static double[][] cowGrowth;
	private static double[][] node3x3cowGrowth;
	private static Tactical9x9Node[][] tactical9x9Nodes;

	private static double totalCowGrowth;

	private MapProcessing() {
	}

	public static void perform() {
		init();
		generateBitmap();
		prepare3x3Nodes();
		prepare9x9Nodes();
		broadcastNavigationData();
		calculateAndBroadcastTacticalData();
		clearMemory();
	}

	private static void calculateAndBroadcastTacticalData() {
		for (int x = width / 9; --x >= 0;) {
			for (int y = height / 9; --y >= 0;) {
				tactical9x9Nodes[x][y] = new Tactical9x9Node();
			}
		}
		totalCowGrowth = 0.0;
		for (int x = RobotPlayer.rc.getMapWidth(); --x >= 0;) {
			for (int y = RobotPlayer.rc.getMapHeight(); --y >= 0;) {
				node3x3cowGrowth[x / 3][y / 3] += cowGrowth[x][y];
				tactical9x9Nodes[x / 9][y / 9].cowGrowth += cowGrowth[x][y];
				totalCowGrowth += cowGrowth[x][y];
				;
			}
		}
		// I'm not very good at implementing graph algorithms, it looks terrible
		// :(
		int distance = 0;
		Queue<NavigationNode> bfsQueue;
		Queue<NavigationNode> nextIterationBfsQueue = new LinkedList<NavigationNode>();
		MapLocation hqLoc = RobotPlayer.rc.senseHQLocation();
		nextIterationBfsQueue.add(graph9x9Nodes[hqLoc.x / 9][hqLoc.y / 9]);
		while (!nextIterationBfsQueue.isEmpty()) {
			distance++;
			bfsQueue = nextIterationBfsQueue;
			nextIterationBfsQueue = new LinkedList<NavigationNode>();
			while (!bfsQueue.isEmpty()) {
				NavigationNode node = bfsQueue.poll();
				tactical9x9Nodes[node.x][node.y].distanceToHQ = distance;
				for (int i = 8; --i >= 0;) {
					Direction dir = Direction.values()[i];
					if (node.isConnected(dir)) {
						NavigationNode consideredNode = graph9x9Nodes[node.x
								+ dir.dx][node.y + dir.dy];
						if (tactical9x9Nodes[consideredNode.x][consideredNode.y].distanceToHQ <= 0) {
							nextIterationBfsQueue.add(consideredNode);
						}
					}
				}
			}
		}
		// Now some broadcasting
		ChannelType channel;
		channel = ChannelType.COW_GROWTH_3x3NODES;
		for (int x = width / 3; --x >= 0;) {
			for (int y = height / 3; --y >= 0;) {
				channel.write(x + 36 * y, (int) node3x3cowGrowth[x][y]);
			}
		}
		channel = ChannelType.TACTICAL_DATA_9x9NODES;
		for (int x = width / 9; --x >= 0;) {
			for (int y = height / 9; --y >= 0;) {
				channel.write(x + 12 * y, tactical9x9Nodes[x][y].write());
			}
		}
		channel = ChannelType.TOTAL_COW_GROWTH;
		channel.write(0, (int) totalCowGrowth);
	}

	private static void init() {
		width = RobotPlayer.rc.getMapWidth()
				+ (RobotPlayer.rc.getMapWidth() % 9 == 0 ? 0
						: 9 - (RobotPlayer.rc.getMapWidth() % 9));
		height = RobotPlayer.rc.getMapHeight()
				+ (RobotPlayer.rc.getMapHeight() % 9 == 0 ? 0
						: 9 - (RobotPlayer.rc.getMapHeight() % 9));

		bitmap = new boolean[width][height];
		graph3x3Nodes = new NavigationNode[width / 3][height / 3];
		graph9x9Nodes = new NavigationNode[width / 9][height / 9];

		cowGrowth = RobotPlayer.rc.senseCowGrowth();
		node3x3cowGrowth = new double[width / 3][height / 3];
		tactical9x9Nodes = new Tactical9x9Node[width / 9][height / 9];
	}

	private static void clearMemory() {
		bitmap = null;
		graph3x3Nodes = null;
		graph9x9Nodes = null;
	}

	private static void generateBitmap() {
		for (int x = width; x-- > 0;) {
			for (int y = height; y-- > 0;) {
				TerrainTile tile = RobotPlayer.rc
						.senseTerrainTile(new MapLocation(x, y));
				bitmap[x][y] = tile.isTraversableAtHeight(RobotLevel.ON_GROUND);
			}
		}
		//Just some hacky enemy HQ avoidance...
		for (MapLocation location : MapLocation
				.getAllMapLocationsWithinRadiusSq(
						RobotPlayer.rc.senseEnemyHQLocation(), 32)) {
			int x = location.x;
			int y = location.y;
			if (x >= 0 && x < width && y >= 0 && y < height) {
				bitmap[x][y] = false;
			}
		}
	}

	private static void prepare3x3Nodes() {
		Node3x3Data[][] temp = new Node3x3Data[width / 3][height / 3];
		int xValue = (width / 3) - 1;
		int yValue = (height / 3) - 1;
		for (int x = width / 3; --x >= 0;) {
			for (int y = height / 3; --y >= 0;) {
				Node3x3Data data = new Node3x3Data();
				NavigationNode node = new NavigationNode(x, y);
				data.calculate(x, y);
				if (data.isTraversable()) {
					if (x < xValue) {
						if (y < yValue) {
							if (y > 0) {
								Node3x3Data upper_right = temp[x + 1][y - 1];
								if (data.reachable[2][0]
										&& upper_right.reachable[0][2]) {
									node.createConnection(Direction.NORTH_EAST);
									graph3x3Nodes[x + 1][y - 1]
											.createConnection(Direction.SOUTH_WEST);
								}
							}
							Node3x3Data right = temp[x + 1][y];
							if ((data.reachable[2][0] && (right.reachable[0][0] || right.reachable[0][1]))
									|| (data.reachable[2][1] && (right.reachable[0][0]
											|| right.reachable[0][1] || right.reachable[0][2]))
									|| (data.reachable[2][2] && (right.reachable[0][1] || right.reachable[0][2]))) {
								node.createConnection(Direction.EAST);
								graph3x3Nodes[x + 1][y]
										.createConnection(Direction.WEST);
							}
							Node3x3Data bottom_right = temp[x + 1][y + 1];
							if (data.reachable[2][2]
									&& bottom_right.reachable[0][0]) {
								node.createConnection(Direction.SOUTH_EAST);
								graph3x3Nodes[x + 1][y + 1]
										.createConnection(Direction.NORTH_WEST);
							}
							Node3x3Data bottom = temp[x][y + 1];
							if ((data.reachable[0][2] && (bottom.reachable[0][0] || bottom.reachable[1][0]))
									|| (data.reachable[1][2] && (bottom.reachable[0][0]
											|| bottom.reachable[1][0] || bottom.reachable[2][0]))
									|| (data.reachable[2][2] && (bottom.reachable[1][0] || bottom.reachable[2][0]))) {
								node.createConnection(Direction.SOUTH);
								graph3x3Nodes[x][y + 1]
										.createConnection(Direction.NORTH);
							}
						} else {
							Node3x3Data right = temp[x + 1][y];
							if ((data.reachable[2][0] && (right.reachable[0][0] || right.reachable[0][1]))
									|| (data.reachable[2][1] && (right.reachable[0][0]
											|| right.reachable[0][1] || right.reachable[0][2]))
									|| (data.reachable[2][2] && (right.reachable[0][1] || right.reachable[0][2]))) {
								node.createConnection(Direction.EAST);
								graph3x3Nodes[x + 1][y]
										.createConnection(Direction.WEST);
							}
						}
					} else if (y < yValue) {
						Node3x3Data bottom = temp[x][y + 1];
						if ((data.reachable[0][2] && (bottom.reachable[0][0] || bottom.reachable[1][0]))
								|| (data.reachable[1][2] && (bottom.reachable[0][0]
										|| bottom.reachable[1][0] || bottom.reachable[2][0]))
								|| (data.reachable[2][2] && (bottom.reachable[1][0] || bottom.reachable[2][0]))) {
							node.createConnection(Direction.SOUTH);
							graph3x3Nodes[x][y + 1]
									.createConnection(Direction.NORTH);
						}
					}
				}
				temp[x][y] = data;
				graph3x3Nodes[x][y] = node;
			}
		}
	}

	private static void prepare9x9Nodes() {
		int xValue = (width / 9) - 1;
		int yValue = (width / 9) - 1;
		Node9x9Data[][] temp = new Node9x9Data[width / 9][height / 9];
		for (int x = width / 9; --x >= 0;) {
			for (int y = height / 9; --y >= 0;) {
				Node9x9Data data = new Node9x9Data();
				NavigationNode node = new NavigationNode(x, y);
				data.calculate(x, y);
				// It does look terrible, but at least works... probably
				if (data.reachable[1][1]) {
					if (x < xValue) {
						if (y < yValue) {
							if (y > 0) {
								Node9x9Data upper_right = temp[x + 1][y - 1];
								if (upper_right.reachable[1][1]
										&& data.reachable[2][0]
										&& upper_right.reachable[0][2]) {
									if (data.smallerNodes[2][0]
											.isConnected(Direction.NORTH_EAST)) {
										node.createConnection(Direction.NORTH_EAST);
										graph9x9Nodes[x + 1][y - 1]
												.createConnection(Direction.SOUTH_WEST);
									}
								}
							}
							Node9x9Data right = temp[x + 1][y];
							if (right.reachable[1][1]) {
								if (data.reachable[2][0]) {
									if (right.reachable[0][0]) {
										if (data.smallerNodes[2][0]
												.isConnected(Direction.EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
									if (right.reachable[0][1]) {
										if (data.smallerNodes[2][0]
												.isConnected(Direction.SOUTH_EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
								}
								if (data.reachable[2][1]) {
									if (right.reachable[0][0]) {
										if (data.smallerNodes[2][1]
												.isConnected(Direction.NORTH_EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
									if (right.reachable[0][1]) {
										if (data.smallerNodes[2][1]
												.isConnected(Direction.EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
									if (right.reachable[0][2]) {
										if (data.smallerNodes[2][1]
												.isConnected(Direction.SOUTH_EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
								}
								if (data.reachable[2][2]) {
									if (right.reachable[0][1]) {
										if (data.smallerNodes[2][2]
												.isConnected(Direction.NORTH_EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
									if (right.reachable[0][2]) {
										if (data.smallerNodes[2][2]
												.isConnected(Direction.EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
								}
							}
							Node9x9Data bottom_right = temp[x + 1][y + 1];
							if (bottom_right.reachable[1][1]
									&& data.reachable[2][2]
									&& bottom_right.reachable[0][0]) {
								if (data.smallerNodes[2][2]
										.isConnected(Direction.SOUTH_EAST)) {
									node.createConnection(Direction.SOUTH_EAST);
									graph9x9Nodes[x + 1][y + 1]
											.createConnection(Direction.NORTH_WEST);
								}
							}
							Node9x9Data bottom = temp[x][y + 1];
							if (bottom.reachable[1][1]) {
								if (data.reachable[0][2]) {
									if (bottom.reachable[0][0]) {
										if (data.smallerNodes[0][2]
												.isConnected(Direction.SOUTH)) {
											node.createConnection(Direction.SOUTH);
											graph9x9Nodes[x][y + 1]
													.createConnection(Direction.NORTH);
										}
									}
									if (bottom.reachable[1][0]) {
										if (data.smallerNodes[0][2]
												.isConnected(Direction.SOUTH_EAST)) {
											node.createConnection(Direction.SOUTH);
											graph9x9Nodes[x][y + 1]
													.createConnection(Direction.NORTH);
										}
									}
								}
								if (data.reachable[1][2]) {
									if (bottom.reachable[0][0]) {
										if (data.smallerNodes[1][2]
												.isConnected(Direction.SOUTH_WEST)) {
											node.createConnection(Direction.SOUTH);
											graph9x9Nodes[x][y + 1]
													.createConnection(Direction.NORTH);
										}
									}
									if (bottom.reachable[1][0]) {
										if (data.smallerNodes[1][2]
												.isConnected(Direction.SOUTH)) {
											node.createConnection(Direction.SOUTH);
											graph9x9Nodes[x][y + 1]
													.createConnection(Direction.NORTH);
										}
									}
									if (bottom.reachable[2][0]) {
										if (data.smallerNodes[1][2]
												.isConnected(Direction.SOUTH_EAST)) {
											node.createConnection(Direction.SOUTH);
											graph9x9Nodes[x][y + 1]
													.createConnection(Direction.NORTH);
										}
									}
								}
								if (data.reachable[2][2]) {
									if (bottom.reachable[1][2]) {
										if (data.smallerNodes[2][2]
												.isConnected(Direction.SOUTH_WEST)) {
											node.createConnection(Direction.SOUTH);
											graph9x9Nodes[x][y + 1]
													.createConnection(Direction.NORTH);
										}
									}
									if (bottom.reachable[2][2]) {
										if (data.smallerNodes[2][2]
												.isConnected(Direction.SOUTH)) {
											node.createConnection(Direction.SOUTH);
											graph9x9Nodes[x][y + 1]
													.createConnection(Direction.NORTH);
										}
									}
								}
							}
						} else {
							Node9x9Data right = temp[x + 1][y];
							if (right.reachable[1][1]) {
								if (data.reachable[2][0]) {
									if (right.reachable[0][0]) {
										if (data.smallerNodes[2][0]
												.isConnected(Direction.EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
									if (right.reachable[0][1]) {
										if (data.smallerNodes[2][0]
												.isConnected(Direction.SOUTH_EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
								}
								if (data.reachable[2][1]) {
									if (right.reachable[0][0]) {
										if (data.smallerNodes[2][1]
												.isConnected(Direction.NORTH_EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
									if (right.reachable[0][1]) {
										if (data.smallerNodes[2][1]
												.isConnected(Direction.EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
									if (right.reachable[0][2]) {
										if (data.smallerNodes[2][1]
												.isConnected(Direction.SOUTH_EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
								}
								if (data.reachable[2][2]) {
									if (right.reachable[0][1]) {
										if (data.smallerNodes[2][2]
												.isConnected(Direction.NORTH_EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
									if (right.reachable[0][2]) {
										if (data.smallerNodes[2][2]
												.isConnected(Direction.EAST)) {
											node.createConnection(Direction.EAST);
											graph9x9Nodes[x + 1][y]
													.createConnection(Direction.WEST);
										}
									}
								}
							}
						}
					} else if (y < yValue) {
						Node9x9Data bottom = temp[x][y + 1];
						if (bottom.reachable[1][1]) {
							if (data.reachable[0][2]) {
								if (bottom.reachable[0][0]) {
									if (data.smallerNodes[0][2]
											.isConnected(Direction.SOUTH)) {
										node.createConnection(Direction.SOUTH);
										graph9x9Nodes[x][y + 1]
												.createConnection(Direction.NORTH);
									}
								}
								if (bottom.reachable[1][0]) {
									if (data.smallerNodes[0][2]
											.isConnected(Direction.SOUTH_EAST)) {
										node.createConnection(Direction.SOUTH);
										graph9x9Nodes[x][y + 1]
												.createConnection(Direction.NORTH);
									}
								}
							}
							if (data.reachable[1][2]) {
								if (bottom.reachable[0][0]) {
									if (data.smallerNodes[1][2]
											.isConnected(Direction.SOUTH_WEST)) {
										node.createConnection(Direction.SOUTH);
										graph9x9Nodes[x][y + 1]
												.createConnection(Direction.NORTH);
									}
								}
								if (bottom.reachable[1][0]) {
									if (data.smallerNodes[1][2]
											.isConnected(Direction.SOUTH)) {
										node.createConnection(Direction.SOUTH);
										graph9x9Nodes[x][y + 1]
												.createConnection(Direction.NORTH);
									}
								}
								if (bottom.reachable[2][0]) {
									if (data.smallerNodes[1][2]
											.isConnected(Direction.SOUTH_EAST)) {
										node.createConnection(Direction.SOUTH);
										graph9x9Nodes[x][y + 1]
												.createConnection(Direction.NORTH);
									}
								}
							}
							if (data.reachable[2][2]) {
								if (bottom.reachable[1][2]) {
									if (data.smallerNodes[2][2]
											.isConnected(Direction.SOUTH_WEST)) {
										node.createConnection(Direction.SOUTH);
										graph9x9Nodes[x][y + 1]
												.createConnection(Direction.NORTH);
									}
								}
								if (bottom.reachable[2][2]) {
									if (data.smallerNodes[2][2]
											.isConnected(Direction.SOUTH)) {
										node.createConnection(Direction.SOUTH);
										graph9x9Nodes[x][y + 1]
												.createConnection(Direction.NORTH);
									}
								}
							}
						}
					}
				}

				temp[x][y] = data;
				graph9x9Nodes[x][y] = node;
			}
		}
	}

	private static void broadcastNavigationData() {
		ChannelType channel;
		channel = ChannelType.NAVIGATION_GRAPH_3x3NODES;
		for (int x = width / 3; --x >= 0;) {
			for (int y = height / 3; --y >= 0;) {
				channel.write(x + 36 * y, graph3x3Nodes[x][y].save());
			}
		}
		channel = ChannelType.NAVIGATION_GRAPH_9x9NODES;
		for (int x = width / 9; --x >= 0;) {
			for (int y = height / 9; --y >= 0;) {
				channel.write(x + 12 * y, graph9x9Nodes[x][y].save());
			}
		}

		channel = ChannelType.NAVIGATION_DATA;
		channel.write(0, new NavigationData(width, height).write());
	}

	private static class Node3x3Data {
		private boolean[][] reachable = new boolean[3][3];

		public void calculate(int x, int y) {
			x *= 3;
			y *= 3;
			for (int i = 3; --i >= 0;) {
				for (int j = 3; --j >= 0;) {
					reachable[i][j] = bitmap[x + i][y + j];
				}
			}
			if (!isTraversable()) {
				reachable = new boolean[3][3];
			}
		}

		public boolean isTraversable() {
			return reachable[1][1];
		}
	}

	private static class Node9x9Data {
		private NavigationNode[][] smallerNodes = new NavigationNode[3][3];
		private boolean[][] reachable = new boolean[3][3];

		public void calculate(int x, int y) {
			x *= 3;
			y *= 3;
			for (int i = 3; --i >= 0;) {
				for (int j = 3; --j >= 0;) {
					smallerNodes[i][j] = graph3x3Nodes[x + i][y + j];
				}
			}
			Direction[] dirs = Direction.values();
			boolean traversable = false;
			Queue<NavigationNode> bfsQueue = new LinkedList<NavigationNode>();
			bfsQueue.add(smallerNodes[1][1]);
			while (!bfsQueue.isEmpty()) {
				NavigationNode consideredNode = bfsQueue.poll();
				int nodeX = consideredNode.x - smallerNodes[0][0].x;
				int nodeY = consideredNode.y - smallerNodes[0][0].y;
				reachable[nodeX][nodeY] = true;
				for (int i = 8; --i >= 0;) {
					if (consideredNode.connected[i]) {
						traversable = true;
						int newX = nodeX + dirs[i].dx;
						int newY = nodeY + dirs[i].dy;
						if (newX >= 0 && newX < 3 && newY >= 0 && newY < 3
								&& !reachable[newX][newY]) {
							bfsQueue.add(smallerNodes[newX][newY]);
						}
					}
				}
			}
			if (!traversable) {
				reachable = new boolean[3][3];
			}
		}
	}
}