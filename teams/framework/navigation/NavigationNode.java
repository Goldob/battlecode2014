package framework.navigation;

import battlecode.common.Direction;

public class NavigationNode {
	public boolean[] connected = new boolean[8];
	public int x;
	public int y;

	public NavigationNode(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void createConnection(Direction direction) {
		connected[direction.ordinal()] = true;
	}

	public boolean isConnected(Direction direction) {
		int ordinal = direction.ordinal();
		if (ordinal <= 7) {
			return connected[ordinal];
		}
		return false;
	}

	public void read(int data) {
		for (int i = 8; --i >= 0;) {
			connected[i] = (data & (1 << i)) != 0;
		}
	}

	public int save() {
		int savedInt = 0;
		for (int i = 8; --i >= 0;) {
			if (connected[i]) {
				savedInt = savedInt | (1 << i);
			}
		}
		return savedInt;
	}
}
