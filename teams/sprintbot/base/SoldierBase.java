package sprintbot.base;

import sprintbot.RobotPlayer;
import sprintbot.behavior.Combat;
import sprintbot.behavior.Movement;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;

public class SoldierBase {
	private static SoldierState currentState = SoldierState.NOT_INITIATED;
	private static int temp = 7;

	public static void init() {
		Movement.init();
		try {
			int data = RobotPlayer.rc.readBroadcast(1);
			RobotPlayer.rc.broadcast(1, data+1);
			if (data == 5) {
				currentState = SoldierState.BUILD_FIRST;
			} else if (data == 7) {
				currentState = SoldierState.BUILD_SECOND;
			} else if (data == 9) {
				currentState = SoldierState.BUILD_THIRD;
			} else {
				currentState = SoldierState.RUSH;
			}
		} catch (GameActionException e) {
			// TODO
		}
	}

	public static void nextRound() {
		// TODO Each round calculations and decisionmaking
		MapLocation location = RobotPlayer.rc.getLocation();
		if (RobotPlayer.rc.isActive()) {
			if (Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 35).length > 0) {
				Combat.killEnemy();
			} else {
				Direction toEnemy = RobotPlayer.rc.getLocation().directionTo(
						RobotPlayer.rc.senseEnemyHQLocation());
				if(currentState == SoldierState.RUSH) {
					MapLocation[] pastrLocations = RobotPlayer.rc.sensePastrLocations(RobotPlayer.rc.getTeam().opponent());
					MapLocation best = RobotPlayer.rc.senseEnemyHQLocation();
					int distance = 0;
					for(MapLocation loc : pastrLocations) {
						if(RobotPlayer.rc.senseEnemyHQLocation().distanceSquaredTo(loc) > distance) {
							distance = RobotPlayer.rc.senseEnemyHQLocation().distanceSquaredTo(loc);
							best = loc;
						}
					}
					Movement.safeMovement(location.directionTo(best), false);
				} else {
					if(RobotPlayer.rc.senseEnemyHQLocation().distanceSquaredTo(location) <= 2*RobotPlayer.rc.senseHQLocation().distanceSquaredTo(location)) {
						if(currentState == SoldierState.BUILD_FIRST) {
							if(temp > 0) {
								temp--;
								Movement.safeMovement(toEnemy.rotateLeft().rotateLeft(), false);
							}
							else {
								try {
									RobotPlayer.rc.construct(RobotType.PASTR);
								} catch (GameActionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else if(currentState == SoldierState.BUILD_SECOND) {
							if(temp > 0) {
								temp--;
								Movement.safeMovement(toEnemy.rotateRight().rotateRight(), false);
							}
							else {
								try {
									RobotPlayer.rc.construct(RobotType.PASTR);
								} catch (GameActionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else if(currentState == SoldierState.BUILD_THIRD) {
							if(temp > 0) {
								temp--;
								Movement.safeMovement(toEnemy.opposite(), false);
							}
							else {
								try {
									RobotPlayer.rc.construct(RobotType.NOISETOWER);
								} catch (GameActionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					} else {
						Movement.safeMovement(toEnemy, false);
					}
				}
			}
		}
	}

	private static enum SoldierState {
		NOT_INITIATED, RUSH, BUILD_FIRST, BUILD_SECOND, BUILD_THIRD;
	}
}
