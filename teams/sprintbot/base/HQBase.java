package sprintbot.base;

import sprintbot.RobotPlayer;
import sprintbot.behavior.Combat;
import battlecode.common.Direction;
import battlecode.common.GameActionException;

public class HQBase {
	public static void init() {
	}
	
	public static void nextRound() {
		//TODO Each round calculations

		if(RobotPlayer.rc.isActive()) {
			if(Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 16).length > 0) {
				Combat.hqDefense();
			} else if(RobotPlayer.rc.senseRobotCount() < 25){ //TODO Check if robot count isn't needed to planned PASTRs and Noise towers
				Direction toEnemy = RobotPlayer.rc.getLocation().directionTo(RobotPlayer.rc.senseEnemyHQLocation());
				tryToSpawn(toEnemy);
			}
		}
	}

	private static void tryToSpawn(Direction toEnemy) {
		recurencySpawn(toEnemy, true, 8);
	}
	
	private static void recurencySpawn(Direction direction, boolean turnRight, int ticks) {
		ticks--;
		try {
			if(RobotPlayer.rc.senseObjectAtLocation(RobotPlayer.rc.getLocation().add(direction)) == null) {
				try {
					RobotPlayer.rc.spawn(direction);
				} catch (GameActionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				recurencySpawn((turnRight ? direction.rotateRight() : direction.rotateLeft()), turnRight, ticks);
			}
		} catch (GameActionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
