package sprintbot.base;

import sprintbot.RobotPlayer;
import sprintbot.behavior.Herding;

public class NoiseTowerBase {
	public static void init() {
		Herding.init(400);
	}
	
	public static void nextRound() {
		if(!Herding.isWorking()) {
			Herding.autoPrepareTargets();
		}
		if(RobotPlayer.rc.isActive()) {
			Herding.perform();
		}	
	}
}
