package sprintbot.messaging;

import sprintbot.navigation.NavData;
import battlecode.common.MapLocation;

public enum ChannelType {
	TASK_1(0),
	TASK_2(10001),
	TASK_3(20002),
	TASK_4(30003),
	TASK_5(40004),
	TASK_6(50005);
	
	//TODO Enemy spotted reports & SOS Channels from channel 60006
	
	private final int index;
	
	ChannelType(int index) {
		this.index = index;
	}
	
	/* Only on task-related channels */
	public NavData readNavData(MapLocation location) {
		//TODO
		return null;
	}
}
