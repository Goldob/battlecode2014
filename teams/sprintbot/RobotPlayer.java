package sprintbot;

import sprintbot.base.HQBase;
import sprintbot.base.NoiseTowerBase;
import sprintbot.base.PASTRBase;
import sprintbot.base.SoldierBase;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class RobotPlayer {
	public static RobotController rc = null;
	
	public static void run(RobotController robotController) {
		rc = robotController;
		while(true) {
			RobotType type = rc.getType();
			if(type == RobotType.HQ) {
				HQBase.init();
				while(rc.getType() == type) {
					HQBase.nextRound();
					rc.yield();
				}
			}
			if(type == RobotType.NOISETOWER || rc.getConstructingType() == RobotType.NOISETOWER) {
				NoiseTowerBase.init();
				while(rc.getType() == type) {
					NoiseTowerBase.nextRound();
					rc.yield();
				}
			}
			if(type == RobotType.PASTR || rc.getConstructingType() == RobotType.PASTR) {
				PASTRBase.init();
				while(rc.getType() == type) {
					PASTRBase.nextRound();
					rc.yield(); //Does PASTR really need that?
				}
			}
			if(type == RobotType.SOLDIER) {
				SoldierBase.init();
				while(rc.getType() == type && !rc.isConstructing()) {
					SoldierBase.nextRound();
					rc.yield();
				}
			}
		}
	}
}
