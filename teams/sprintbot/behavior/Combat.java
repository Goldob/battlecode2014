package sprintbot.behavior;

import sprintbot.RobotPlayer;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class Combat {
	/*Simple combat code for soldiers
	 * TODO - Flee and send SOS message if battle is lost*/
	public static void killEnemy() {
		MapLocation location = RobotPlayer.rc.getLocation();
		MapLocation target;
		if(enemiesInRange(location, 10).length > 0) {
			target = pickTarget(location, 10, 10);
		}
		else {
			target = pickTarget(location, 10, 35);
		}
		if(target == null) {
			//TODO Maybe bigger range?
			return;
		}
		try {
			if(RobotPlayer.rc.canAttackSquare(target)) {
				RobotPlayer.rc.attackSquare(target);
			}
			else {
				Movement.safeMovement(location.directionTo(target), false);
			}
		} catch (GameActionException e) {
			//TODO
			e.printStackTrace();
		}
	}

	public static void hqDefense() {
		int value = 0;
		MapLocation bestTarget = null;
		for(MapLocation consideredTarget : MapLocation.getAllMapLocationsWithinRadiusSq(RobotPlayer.rc.getLocation(), 15)) {
			int consideredValue = 0;
			try {
				Robot inLocation = (Robot)RobotPlayer.rc.senseObjectAtLocation(consideredTarget);
				if(inLocation != null) {
					if(inLocation.getTeam() == RobotPlayer.rc.getTeam()) {
						consideredValue -= calculateValue(inLocation, 2);
					}
					else {
						consideredValue += calculateValue(inLocation, 2);
					}
				}
				for(Robot ally : alliesInRange(consideredTarget, 2)) {
					consideredValue -= calculateValue(ally, 1);
				}
				for(Robot enemy : alliesInRange(consideredTarget, 2)) {
					consideredValue += calculateValue(enemy, 1);
				}
				if(consideredValue > value) {
					bestTarget = consideredTarget;
					value = consideredValue;
				}
			} catch (GameActionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(bestTarget != null) {
			try {
				RobotPlayer.rc.attackSquare(bestTarget);
			} catch (GameActionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/*Pick the best target, including importance, HP and distance*/
	//TODO Better exception handling
	private static MapLocation pickTarget(MapLocation location, int attackRangeSquared, int selectionRangeSquared) {
		double value = 0.0;
		MapLocation bestTarget = null;
		if(selectionRangeSquared > attackRangeSquared) {
			for(Robot enemy : enemiesInRange(location, selectionRangeSquared)) {
				try {
					if(location.distanceSquaredTo(RobotPlayer.rc.senseLocationOf(enemy)) <= attackRangeSquared) {
						continue;
					}
				} catch (GameActionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				double consideredValue = 0;
				try {
					consideredValue = calculateValue(enemy, selectionRangeSquared+1.0-location.distanceSquaredTo(RobotPlayer.rc.senseLocationOf(enemy)));
				} catch (GameActionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} //TODO Some number here, based on distance
				if(consideredValue > value) {
					try {
						bestTarget = RobotPlayer.rc.senseLocationOf(enemy);
						value = consideredValue;
					} catch (GameActionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					value = consideredValue;
				}
			}
		}
		for(Robot enemy : enemiesInRange(location, attackRangeSquared)) {
			double consideredValue = calculateValue(enemy, 1.0); //10 is soldier damage
			if(consideredValue > value) {
				try {
					bestTarget = RobotPlayer.rc.senseLocationOf(enemy);
					value = consideredValue;
				} catch (GameActionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				value = consideredValue;
			}
		}
		
		return bestTarget;
	}
	
	private static double calculateValue(Robot target, double multiply) {
		RobotInfo targetData = null;
		try {
			targetData = RobotPlayer.rc.senseRobotInfo(target);
		} catch (GameActionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(targetData.type == RobotType.HQ) {
			return 0.00001;
		} 
		else if(targetData.type == RobotType.NOISETOWER) {
			return multiply*(100-targetData.health)/100;
		} 
		else if(targetData.type == RobotType.SOLDIER) {
			return multiply*(5.0+(100-targetData.health)/100);
		}
		else {
			return multiply*4.0+((200-targetData.health)/200);
		}
	}
	
	public static Robot[] enemiesInRange(MapLocation location, int rangeSquared) {
		return RobotPlayer.rc.senseNearbyGameObjects(Robot.class, location, rangeSquared, RobotPlayer.rc.getTeam().opponent());
	}
	
	public static Robot[] alliesInRange(MapLocation location, int rangeSquared) {
		return RobotPlayer.rc.senseNearbyGameObjects(Robot.class, location, rangeSquared, RobotPlayer.rc.getTeam());
	}
	
	/*Checks if we can got hit by HQ (TODO does it consider splash damage?)*/
	public static boolean inRangeOfEnemyHQ(MapLocation location) {
		return location.distanceSquaredTo(RobotPlayer.rc.senseEnemyHQLocation()) <= 16;
	}
}
