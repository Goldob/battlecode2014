package sprintbot.behavior;

import sprintbot.RobotPlayer;
import battlecode.common.Direction;
import battlecode.common.GameActionException;

public class Movement {
	private static Direction lastDirectionMoved = Direction.NONE;
	private static boolean turnRight = true;
	
	public static void init() {
		if(RobotPlayer.rc.getRobot().getID()%2 == 1) {
			turnRight = false;
		}
	}
	
	public static void semiBugNav(Direction direction, boolean isSneaking) {
		recurencyBug(direction, isSneaking, false, 8);
	}
	
	public static void safeMovement(Direction direction, boolean isSneaking) {
		recurencyBug(direction, isSneaking, true, 8);
	}
	
	private static void recurencyBug(Direction direction, boolean isSneaking, boolean safeMovement, int ticks) {
		ticks--;
		if(RobotPlayer.rc.canMove(direction) && direction.opposite() != lastDirectionMoved && (!safeMovement || !Combat.inRangeOfEnemyHQ(RobotPlayer.rc.getLocation().add(direction)))) {
			try {
				if(isSneaking) {
					RobotPlayer.rc.sneak(direction);
				}
				else {
					RobotPlayer.rc.move(direction);
				}
				lastDirectionMoved = direction;
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
		else if(ticks > 0) {
			recurencyBug((turnRight ? direction.rotateRight() : direction.rotateLeft()), isSneaking, safeMovement, ticks);
		}
	}
}
