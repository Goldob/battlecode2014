package seedingbot.task;

import java.util.LinkedList;
import java.util.Queue;

import seedingbot.RobotPlayer;
import seedingbot.behavior.Combat;
import seedingbot.messaging.ChannelType;

public class TaskSystem {
	private static Task performedTask;
	private static int taskID;
	private static Queue<Task> scheduledTasks;
	
	private TaskSystem() {}
	
	public static void init() {
		performedTask = null;
		taskID = -1;
		scheduledTasks = new LinkedList<Task>();
	}
	
	public static boolean isPerformingTask() {
		if(performedTask != null) {
			if(performedTask.isExpired()) {
				performedTask = null;
				TaskSystem.setTaskState(taskID, TaskState.FAILURE);
				taskID = -1;
				return false;
			}
			return true;
		}
		return false;
	}

	public static boolean findNewTask() {
		if(performedTask != null && isPerformingTask()) {
			return false;
		}
		for(int i = 5; --i >= 0;) {
			if(getTaskState(i) == TaskState.READY) {
				performedTask = getTask(i);
				taskID = i;
				setTaskState(i, TaskState.IN_PROGRESS);
				return true;
			}
		}
		return false;
	}
	
	public static void taskCalculations() {
		if(performedTask != null) {
			performedTask.calculate();
		}
	}
	
	public static void performTask() {
		if(performedTask != null) {
			if(performedTask.perform()) {
				setTaskState(taskID, TaskState.SUCCESS);
				performedTask = null;
				taskID = -1;
			}
		} else if(Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 35).length > 0) {
			Combat.retreat(RobotPlayer.rc.senseHQLocation());
		}
	}
	
	public static void scheduleTask(Task task) {
		scheduledTasks.add(task);
	}
	
	public static void update() {
		//TODO Check if any task are completed
		if(!scheduledTasks.isEmpty()) {
			for(int i = 5; --i >= 0;) {
				if(getTaskState(i) == TaskState.NOT_ASSIGNED) {
					setTask(i, scheduledTasks.poll());
					setTaskState(i, TaskState.READY);
					if(scheduledTasks.isEmpty()) break;
				}
			}
		}
	}
	
	public static void setTask(int index, Task task) {
		ChannelType.TASK_SYSTEM.write(2*index, task.toMessage());
	}
	
	public static void setTaskState(int index, TaskState state) {
		ChannelType.TASK_SYSTEM.write(2*index+1, state.getCode());
	}
	
	public static Task getTask(int index) {
		return Task.read(ChannelType.TASK_SYSTEM.read(2*index));
	}
	
	public static TaskState getTaskState(int index) {
		int message = ChannelType.TASK_SYSTEM.read(index*2+1);
		if(message == 0) {
			return TaskState.NOT_ASSIGNED;
		} else if(message == -1) {
			return TaskState.READY;
		} else if(message == -2) {
			return TaskState.SUCCESS;
		} else if(message == -3) {
			return TaskState.FAILURE;
		} else {
			return TaskState.IN_PROGRESS;
		}
	}
	
	public static enum TaskState {
		//Any code higher than 0 is robot's id
		NOT_ASSIGNED(0), READY(-1), SUCCESS(-2), FAILURE(-3),
		IN_PROGRESS {
			@Override
			public int getCode() {
				return RobotPlayer.rc.getRobot().getID();
			}
		};
		
		private int code = Integer.MIN_VALUE;
		
		//Only for IN_PROGRESS state :D
		TaskState() {}
		
		TaskState(int code) {
			this.code = code;
		}
		
		public int getCode() {
			return code;
		}
	}	
}