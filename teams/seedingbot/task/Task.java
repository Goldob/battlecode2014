package seedingbot.task;

import seedingbot.RobotPlayer;
import seedingbot.behavior.Combat;
import seedingbot.behavior.Movement;
import seedingbot.navigation.NavigationSystem;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;

public class Task {
	private MapLocation destination;
	private int expiryRound;
	private TaskType type;
	
	private Direction toWaypoint = null;
	
	public Task(MapLocation destination, int expiryRound, TaskType type) {
		this.destination = destination;
		this.expiryRound = expiryRound;
		this.type = type;
	}
	
	public void calculate() {
		if(Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 35).length == 0) {
			if(RobotPlayer.rc.getLocation().distanceSquaredTo(destination) > 2 && toWaypoint == null) {
				toWaypoint = NavigationSystem.navigate(destination);
			}
		}
	}
	
	public boolean perform() {
		if(RobotPlayer.rc.getLocation().distanceSquaredTo(destination) <= 2) {
			return type.perform();
		} else  {
			if(toWaypoint != null) {
				Movement.tryToMove(toWaypoint, false);
			}
			toWaypoint = null;
			return false;
		}
	}
	
	public boolean isExpired() {
		return Clock.getRoundNum() >= expiryRound;
	}
	
	public int toMessage() {
		return ((byte)type.ordinal() << 24) | ((byte)(expiryRound/10) << 16) | ((byte)destination.x << 8) | (byte)destination.y;
	}
	
	public static Task read(int message) {
		int ordinal = (message >> 24) & 0x000000FF;
		int expiryRound = 10*((message >> 16) & 0x000000FF);
		int x = (message >> 8) & 0x000000FF;
		int y = message & 0x000000FF;
		return new Task(new MapLocation(x, y), expiryRound, TaskType.values()[ordinal]);
	}
	
	public static enum TaskType {
		BUILD_PASTR {
			@Override
			public boolean perform() {
				try {
					RobotPlayer.rc.construct(RobotType.PASTR);
					return true;
				} catch (GameActionException e) {
					e.printStackTrace();
				}
				return false;
			}
		},
		BUILD_NOISETOWER {
			@Override
			public boolean perform() {
				try {
					RobotPlayer.rc.construct(RobotType.NOISETOWER);
					return true;
				} catch (GameActionException e) {
					e.printStackTrace();
				}
				return false;
			}
		},;
		
		//Must return true, if everything is done
		public abstract boolean perform();
	}
}
