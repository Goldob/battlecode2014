package seedingbot.messaging;

import seedingbot.RobotPlayer;
import seedingbot.behavior.CombatRewritten;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class SharedVisionSystem {
	public static EnemyData[] globalData = new EnemyData[0];
	
	private SharedVisionSystem() {};
	
	public static void update() {
		uploadSensedData();
		downloadStoredData();
	}
	
	public static void hqUpdate() {
		uploadSensedData();
		parseData();
		downloadStoredData();
	}
	
	private static void uploadSensedData() {
		int index = ChannelType.VISION_SYSTEM_INPUT_INDEX.read(0);
		boolean indexChanged = false;
		for(Robot enemy : CombatRewritten.enemiesInRange(RobotPlayer.rc.getLocation(), 35)) {
			try {
				RobotInfo info = RobotPlayer.rc.senseRobotInfo(enemy);
				EnemyData data = EnemyData.fromRobotInfo(enemy.getID(), info);
				if(info.type == RobotType.SOLDIER) {
					ChannelType.VISION_SYSTEM_INPUT.write(2*index, data.id);
					ChannelType.VISION_SYSTEM_INPUT.write(2*index+1, data.toMessage());
					index++;
					indexChanged = true;
				}
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
		if(indexChanged) ChannelType.VISION_SYSTEM_INPUT_INDEX.write(0, index);
	}
	
	private static void parseData() {
		EnemyData[] robots = new EnemyData[25];
		int filled = 0;
		MainLoop: for(int i = ChannelType.VISION_SYSTEM_INPUT_INDEX.read(0); --i >= 0;) {
			int id = ChannelType.VISION_SYSTEM_INPUT.read(2*i);
			for(int j = filled; --j >= 0;) {
				if(robots[j].id == id) {
					continue MainLoop;
				}
			}
			robots[filled] = EnemyData.fromMessage(id, ChannelType.VISION_SYSTEM_INPUT.read(2*i+1));
			filled++;
			if(filled == 25) break;
		}
		//TODO Some analysis on broadcasting robots... Or maybe everyone can do this by himself?
		ChannelType.VISION_SYSTEM_INPUT_INDEX.write(0, 0);
		for(int i = filled; --i >= 0;) {
			ChannelType.VISION_SYSTEM_OUTPUT.write(2*i, robots[i].id);
			ChannelType.VISION_SYSTEM_OUTPUT.write(2*i+1, robots[i].toMessage());
		}
		ChannelType.VISION_SYSTEM_OUTPUT_LENGTH.write(0, filled);
	}
	
	private static void downloadStoredData() {
		globalData = new EnemyData[ChannelType.VISION_SYSTEM_OUTPUT_LENGTH.read(0)];
		for(int i = ChannelType.VISION_SYSTEM_OUTPUT_LENGTH.read(0); --i >= 0;) {
			ChannelType channel = ChannelType.VISION_SYSTEM_OUTPUT;
			globalData[i] = EnemyData.fromMessage(channel.read(2*i), channel.read(2*i+1));
		}
		RobotPlayer.rc.setIndicatorString(0, String.valueOf(globalData.length));
	}
	
	public static class EnemyData {
		public int id;
		public int actionDelay;
		public int health;
		public MapLocation location;
		
		private EnemyData(int id, int actionDelay, int health, MapLocation location) {
			this.id = id;
			this.actionDelay = actionDelay;
			this.health = health;
			this.location = location;
		};
		
		public int toMessage() {
			return (((byte)actionDelay) << 24) | (((byte)health) << 16) | (((byte)location.x) << 8) | (byte)location.y;
		}
		
		public static EnemyData fromRobotInfo(int id, RobotInfo info) {
			return new EnemyData(id, (int)info.actionDelay, (int)info.health, info.location);
		}
		
		public static EnemyData fromMessage(int id, int message) {
			int actionDelay = (message >> 24) & 0x000000FF;
			int health = (message >> 16) & 0x000000FF;
			int x = (message >> 8) & 0x000000FF;
			int y = message & 0x000000FF;
			return new EnemyData(id, actionDelay, health, new MapLocation(x, y));
		}
	}
}
