package seedingbot.messaging;

import seedingbot.RobotPlayer;
import battlecode.common.GameActionException;

public enum ChannelType {
	NAVIGATION_DATA(0, 0),
	NAVIGATION_GRAPH_3x3NODES(1, 1296),
	NAVIGATION_GRAPH_9x9NODES(1297, 1440),
	
	//TODO SWARM_LOBBY or sth
	SWARM_LEADER(1441, 1446), //3 IDs and 3 locations
	SWARM_ORDERS(1447, 1449),
	
	VISION_SYSTEM_INPUT_INDEX(1450, 1450),
	VISION_SYSTEM_INPUT(1451, 2800), //Up to 625 alerts per round? (25*25) (ID, then data)
	
	VISION_SYSTEM_OUTPUT_LENGTH(2801, 2801),
	VISION_SYSTEM_OUTPUT(2802, 2826),
	
	TASK_SYSTEM(5000, 5009); //5 tasks & 5 robot IDs/task states
	
	private final int FIRST_INDEX;
	private final int LAST_INDEX;
	
	ChannelType(final int FIRST_INDEX, final int LAST_INDEX) {
		this.FIRST_INDEX = FIRST_INDEX;
		this.LAST_INDEX = LAST_INDEX;
	}
	
	public void write(int index, int message) {
		index += FIRST_INDEX;
		if(index <= LAST_INDEX && index >= FIRST_INDEX) {
			try {
				RobotPlayer.rc.broadcast(index, message);
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
	}
	
	public int read(int index) {
		index += FIRST_INDEX;
		if(index <= LAST_INDEX && index >= FIRST_INDEX) {
			try {
				return RobotPlayer.rc.readBroadcast(index);
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
}