package seedingbot.behavior;

import java.util.LinkedList;
import java.util.List;

import seedingbot.RobotPlayer;
import seedingbot.behavior.Combat.HQAvoidance;
import seedingbot.behavior.Movement.DirectionValidator;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TerrainTile;

public class CombatRewritten {
	//TODO Maybe some cow killing
	public static void killEnemies(MapLocation target) {
		MapLocation location = RobotPlayer.rc.getLocation();
		Robot[] enemiesInRange = enemiesInRange(location, 10);
		if(enemiesInRange.length > 0) {
			if(enemiesInRange.length > alliesInRange(location, 35).length || RobotPlayer.rc.getHealth() <= 30) { //TODO check if it's not only PASTRs
				int soldierCount = 0;
				for(Robot enemy : enemiesInRange) {
					try {
						if(RobotPlayer.rc.senseRobotInfo(enemy).type == RobotType.SOLDIER) soldierCount++;
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				}
				if(soldierCount > alliesInRange(location, 35).length || (soldierCount != 0 && RobotPlayer.rc.getHealth() <= 30)) {
					retreat(target);
					return;
				}
				//TODO yoloBomber();
			}
			pickTarget();
		} else {
			ContextMap combatMap = new ContextMap();
			for(Robot enemy : enemiesInRange(location, 35)) {
				Direction toEnemy = Direction.NONE;
				double distanceFactor = 1.0;
				try {
					MapLocation enemyLoc = RobotPlayer.rc.senseLocationOf(enemy);
					distanceFactor = 35.0/((enemyLoc.distanceSquaredTo(location)-10));
					toEnemy = location.directionTo(enemyLoc);
				} catch (GameActionException e) {
					e.printStackTrace();
				}
				double interest = calculateInterest(enemy, distanceFactor);
				double danger = calculateDanger(enemy, distanceFactor);
				
				combatMap.putDanger(toEnemy, danger);
				combatMap.putInterest(toEnemy, interest);
				
				combatMap.putDanger(toEnemy.rotateLeft(), danger/2);
				combatMap.putInterest(toEnemy.rotateLeft(), interest/2);
				
				combatMap.putDanger(toEnemy.rotateRight(), danger/2);
				combatMap.putInterest(toEnemy.rotateRight(), interest/2);
			}
			//TODO Maybe calculate interest for nearby allies?
			Direction best = Direction.NONE;
			double bestValue = 0.0;
			double[] values = combatMap.getValues();
			for(int i = 8; --i >= 0;) {
				if(values[i] >= bestValue && Movement.isValid(Direction.values()[i], new BeCareful())) {
					best = Direction.values()[i];
					bestValue = values[i];
				}
			}
			if(best != Direction.NONE) {
				try {
					RobotPlayer.rc.move(best);
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void retreat(MapLocation destination) {
		int minCount = Integer.MAX_VALUE;
		List<Direction> possibleDirections = new LinkedList<Direction>();
		for(int i = 8; --i >= 0;) {
			Direction dir = Direction.values()[i];
			int enemies = enemiesInRange(RobotPlayer.rc.getLocation().add(dir), 10).length;
			if(enemies < minCount && Movement.isValid(dir, new HQAvoidance())) {
				minCount = enemies;
				possibleDirections = new LinkedList<Direction>();
			}
			if(enemies == minCount && Movement.isValid(dir, new HQAvoidance())) {
				possibleDirections.add(dir);
			}
		}
		Direction toDestination = RobotPlayer.rc.getLocation().directionTo(destination);
		int minDifference = Integer.MAX_VALUE;
		Direction bestDirection = Direction.NONE;
		for(Direction dir : possibleDirections) {
			int difference = Math.abs(toDestination.ordinal()-dir.ordinal());
			if(difference <= minDifference) {
				bestDirection = dir;
				minDifference = difference;
			}
		}
		try {
			RobotPlayer.rc.move(bestDirection);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}
	
	private static void pickTarget() {
		MapLocation best = null;
		double bestValue = 0.0;
		for(Robot enemy : enemiesInRange(RobotPlayer.rc.getLocation(), 10)) {
			double consideredValue = calculateTargetValue(enemy);
			if(consideredValue > bestValue) {
				try {
					best = RobotPlayer.rc.senseLocationOf(enemy);
					bestValue = consideredValue;
				} catch (GameActionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if(best != null) {
			try {
				RobotPlayer.rc.attackSquare(best);
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
	}

	public static void yoloBomber() {
		//TODO, so much to do
		MapLocation location = RobotPlayer.rc.getLocation();
		if(enemiesInRange(location, 2).length > 0) {
			try {
				RobotPlayer.rc.selfDestruct();
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		} else {
			MapLocation closest = null;
			int minDistance = Integer.MAX_VALUE;
			for(Robot enemy : enemiesInRange(location, 10)) {
				try {
					MapLocation enemyLoc = RobotPlayer.rc.senseLocationOf(enemy);
					if(location.distanceSquaredTo(enemyLoc) < minDistance) {
						minDistance = location.distanceSquaredTo(enemyLoc);
						closest = enemyLoc;
					}
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
			Movement.tryToMove(location.directionTo(closest), false);
		}
	}
	
	private static double calculateDanger(Robot robot, double factor) {
		if(robot.getTeam() == RobotPlayer.team) {
			return 0.0;
		} else {
			MapLocation enemyLocation = null;
			try {
				enemyLocation = RobotPlayer.rc.senseLocationOf(robot);
				if(RobotPlayer.rc.senseRobotInfo(robot).type != RobotType.SOLDIER) {
					return 0.0;
				}
			} catch (GameActionException e) {
				e.printStackTrace();
			}
			if(enemyLocation != null) {
				return enemiesInRange(enemyLocation, 10).length/Math.max(1.0, 2*alliesInRange(enemyLocation, 10).length);
			}
			return 0.0;
		}
	}
	
	private static double calculateInterest(Robot robot, double factor) {
		return calculateTargetValue(robot)*factor;
	}
	
	private static double calculateTargetValue(Robot target) {
		RobotInfo targetData = null;
		try {
			targetData = RobotPlayer.rc.senseRobotInfo(target);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		if(targetData.type == RobotType.NOISETOWER) {
			return 1.0+(100-targetData.health)/100;
		} 
		else if(targetData.type == RobotType.SOLDIER) {
			return 5.0+(100-targetData.health)/100;
		}
		else if(targetData.type == RobotType.PASTR) {
			return 4.0+((200-targetData.health)/100);
		}
		return 0.0;
	}
	
	public static Robot[] enemiesInRange(MapLocation location, int rangeSquared) {
		return RobotPlayer.rc.senseNearbyGameObjects(Robot.class, location, rangeSquared, RobotPlayer.rc.getTeam().opponent());
	}
	
	public static Robot[] alliesInRange(MapLocation location, int rangeSquared) {
		return RobotPlayer.rc.senseNearbyGameObjects(Robot.class, location, rangeSquared, RobotPlayer.rc.getTeam());
	}
	
	public static boolean inRangeOfEnemyHQ(MapLocation location) {
		return location.distanceSquaredTo(RobotPlayer.rc.senseEnemyHQLocation()) <= 15;
	}
	
	public static class BeCareful implements DirectionValidator {
		
		/* Returns true, if:
		 * 	a) Doesn't close combat distance to any enemy soldier
		 * 	b) Closes combat distance only to enemy soldiers already in fight
		 * 	c) Closes combat distance, but we heavily outnumber them and the robot has full HP
		 * AND we don't go into HQ range
		 */
		@Override
		public boolean isValid(Direction direction) {
			MapLocation location = RobotPlayer.rc.getLocation();
			Robot[] enemiesInRange = enemiesInRange(location.add(direction), 10);
			if(inRangeOfEnemyHQ(location.add(direction))) {
				return false;
			}
			if(enemiesInRange.length == 0) {
				return true;
			} else if(RobotPlayer.rc.senseTerrainTile(location) == TerrainTile.ROAD && RobotPlayer.rc.getHealth() == 100.0 && alliesInRange(location, 4).length > 2*enemiesInRange.length) {
				return true;
			} else {
				for(Robot enemy : enemiesInRange) {
					try {
						if((alliesInRange(RobotPlayer.rc.senseLocationOf(enemy), 10).length == 0 || RobotPlayer.rc.getHealth() <= 30) && RobotPlayer.rc.senseRobotInfo(enemy).type == RobotType.SOLDIER) {
							return false;
						}
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				}
			}
			return true;
		}
	}
}
