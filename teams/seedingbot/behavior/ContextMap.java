package seedingbot.behavior;

import java.util.Arrays;

import battlecode.common.Direction;

public class ContextMap {
	public double[] interest;
	public double[] danger;
	
	public ContextMap() {
		interest = new double[8];
		danger = new double[8];
		Arrays.fill(interest, 0.0);
		Arrays.fill(danger, 0.0);
	}
	
	public void putInterest(Direction direction, double value) {
		interest[direction.ordinal()] = Math.max(value, interest[direction.ordinal()]);
	}
	
	public void putDanger(Direction direction, double value) {
		danger[direction.ordinal()] = Math.max(value, danger[direction.ordinal()]);
	}
	
	public double[] getValues() {
		double[] values = new double[8];
		for(int i = 8; --i >= 0;) {
			values[i] = interest[i] - danger[i];
		}
		return values;
	}
}
