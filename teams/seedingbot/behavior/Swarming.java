package seedingbot.behavior;

import seedingbot.RobotPlayer;
import seedingbot.behavior.Movement.DirectionValidator;
import seedingbot.messaging.ChannelType;
import seedingbot.messaging.SharedVisionSystem;
import seedingbot.messaging.SharedVisionSystem.EnemyData;
import seedingbot.navigation.NavigationSystem;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;

public class Swarming {
	private static RobotSwarm mySwarm = RobotSwarm.NOT_ASSIGNED;
	private static MapLocation leaderLocation;
	private static SwarmOrder order;
	
	private static Direction nextMove;
	
	public static void init() {
		mySwarm = RobotSwarm.SWARM_1;
		leaderLocation = null;
		order = null;
		nextMove = null;
	}
	
	public static void calculate() {
		order = mySwarm.getOrder();
		//TODO Broadcast spotted enemies
		if(isLeader()) {
			leaderLocation = RobotPlayer.rc.getLocation();
			if(nextMove == null) {
				nextMove = NavigationSystem.navigate(order.location);
			}
			//mySwarm.writeLeaderLocation(leaderLocation);
		} else  {
			leaderLocation = mySwarm.getLeaderLocation();
			if(RobotPlayer.rc.getLocation().distanceSquaredTo(leaderLocation) > 100) {
				nextMove = NavigationSystem.navigate(order.location);
			}
			else {
				nextMove = RobotPlayer.rc.getLocation().directionTo(leaderLocation);
			}
		}
	}
	
	public static void perform() {
		//TODO Make it check also enemy positions reported by mates
		if(Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 35).length > 0) {
			order.combatBehavior();
		} else {
			MapLocation myLocation = RobotPlayer.rc.getLocation();
			for(EnemyData data : SharedVisionSystem.globalData) {
				if(myLocation.distanceSquaredTo(data.location) <= 70) {
					Movement.tryToMove(myLocation.directionTo(data.location), false);
					return;
				}
			}
			if(isLeader()) {
				Movement.tryToMove(nextMove, false);
			} else {
				Movement.tryToMove(nextMove, false, new LeaderSeparation());
			}
			nextMove = null;
		}
	}
	
	public static boolean isLeader() {
		int leaderID = mySwarm.getLeaderID();
		if(leaderID == 0) {
			mySwarm.takeLeadership();
			return true;
		}
		return leaderID == RobotPlayer.rc.getRobot().getID();
	}
	
	public static enum RobotSwarm {
		SWARM_1(0), SWARM_2(1), SWARM_3(2), NOT_ASSIGNED(-1);
		
		private int index;

		RobotSwarm(int index) {
			this.index = index;
		}	
		
		public int getLeaderID() {
			return ChannelType.SWARM_LEADER.read(2*index);
		}
		
		public void setLeaderID(int id) {
			ChannelType.SWARM_LEADER.write(2*index, id);
		}
		
		public MapLocation getLeaderLocation() {
			int message = ChannelType.SWARM_LEADER.read(2*index+1);
			byte x = (byte)((message >> 8) & 0x000000FF);
			byte y = (byte)(message & 0x000000FF);
			return new MapLocation(x, y);
		}
		
		public void takeLeadership() {
			ChannelType.SWARM_LEADER.write(2*index, RobotPlayer.rc.getRobot().getID());
			writeLeaderLocation(RobotPlayer.rc.getLocation());
		}
		
		public void writeLeaderLocation(MapLocation location) {
			int message = (((location.x & 0x000000FF) << 8) | (location.y & 0x000000FF));
			ChannelType.SWARM_LEADER.write(2*index+1, message);
		}
		
		public SwarmOrder getOrder() {
			return SwarmOrder.fromMessage(ChannelType.SWARM_ORDERS.read(index));
		}
		
		public void giveOrder(SwarmOrder order) {
			ChannelType.SWARM_ORDERS.write(index, order.toMessage());
		}
	}
	
	public static class LeaderSeparation implements DirectionValidator {
		@Override
		public boolean isValid(Direction direction) {
			return (RobotPlayer.rc.getLocation().add(direction).distanceSquaredTo(leaderLocation) > 8);
		}
	}
	
	public static class SwarmOrder {
		public OrderType type;
		public MapLocation location;
		
		public SwarmOrder(OrderType type, MapLocation location) {
			this.type = type;
			this.location = location;
		}
		
		public void combatBehavior() {
			type.combatBehavior();
		}

		public static SwarmOrder fromMessage(int message) {
			byte type = (byte)((message >> 16) & 0x000000FF);
			byte x = (byte)((message >> 8) & 0x000000FF);
			byte y = (byte)(message & 0x000000FF);
			return new SwarmOrder(OrderType.values()[type], new MapLocation(x, y));
		}
		
		public int toMessage() {
			return (((type.ordinal() & 0x000000FF) << 16) | ((location.x & 0x000000FF) << 8) | (location.y & 0x000000FF));
		}
	}
	
	public static enum OrderType {
		REGROUP {
			@Override
			public void combatBehavior() {
				//Combat.retreat(order.location);
				CombatRewritten.killEnemies(leaderLocation);
			}
		},
		INVADE {
			@Override
			public void combatBehavior() {
				//Combat.killEverything();
				CombatRewritten.killEnemies(leaderLocation);
			}
		}, DEFEND {
			@Override
			public void combatBehavior() {
				//Combat.defendPosition();
				CombatRewritten.killEnemies(leaderLocation);
			}
		};

		public abstract void combatBehavior();
	}
	
	public static void findSwarmLeaders() {
		findSwarmLeader(RobotSwarm.SWARM_1);
		findSwarmLeader(RobotSwarm.SWARM_2);
		findSwarmLeader(RobotSwarm.SWARM_3);
	}
	
	private static void findSwarmLeader(RobotSwarm swarm) {
		int leaderID = swarm.getLeaderID();
		if(leaderID == 0) return;
		MapLocation leaderLoc = swarm.getLeaderLocation();
		for(Robot robot : RobotPlayer.rc.senseNearbyGameObjects(Robot.class, leaderLoc, 2, RobotPlayer.team)) {
			if(robot.getID() == leaderID) {
				try {
					swarm.writeLeaderLocation(RobotPlayer.rc.senseRobotInfo(robot).location);
					return;
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
		swarm.setLeaderID(0);
	}
}
