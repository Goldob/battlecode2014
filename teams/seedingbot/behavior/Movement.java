package seedingbot.behavior;

import seedingbot.RobotPlayer;
import battlecode.common.Direction;
import battlecode.common.GameActionException;

public class Movement {
	private static int directionalLooks[];
	public static void init() {
		if(RobotPlayer.rc.getRobot().getID()%2 == 1) {
			directionalLooks = new int[]{0,1,-1,2,-2, 3,-3,4};
		} else {
			directionalLooks = new int[]{0,-1,1,-2,2,-3,3,-4};
		}
	}

	public static void tryToMove(Direction direction, boolean isSneaking, DirectionValidator... validators) {
		int forwardInt = direction.ordinal();
		for(int directionalOffset:directionalLooks){
			Direction trialDir = Direction.values()[(forwardInt+directionalOffset+8)%8];
			if(isValid(trialDir, validators)){
				try {
					if(isSneaking) RobotPlayer.rc.sneak(trialDir);
					else RobotPlayer.rc.move(trialDir);
				} catch (GameActionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
		}
	}
	
	public static boolean isValid(Direction direction, DirectionValidator... validators) {
		if (!RobotPlayer.rc.canMove(direction)) return false;
		for(DirectionValidator validator : validators) {
			if(!validator.isValid(direction)){
				return false;
			}	
		}
		return true;
	}
	
	public static interface DirectionValidator {
		public boolean isValid(Direction direction);
	}
}
