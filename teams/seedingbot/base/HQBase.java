package seedingbot.base;

import seedingbot.RobotPlayer;
import seedingbot.behavior.Combat;
import seedingbot.behavior.Swarming;
import seedingbot.behavior.Swarming.OrderType;
import seedingbot.behavior.Swarming.RobotSwarm;
import seedingbot.behavior.Swarming.SwarmOrder;
import seedingbot.messaging.SharedVisionSystem;
import seedingbot.task.Task;
import seedingbot.task.TaskSystem;
import seedingbot.task.Task.TaskType;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotLevel;

public class HQBase {
	public static void init() {
		TaskSystem.init();
	}
	
	public static void nextRound() {
		//TODO Each round calculations
		Swarming.findSwarmLeaders();
		TaskSystem.update();
		if(Clock.getRoundNum() % 5 == 1) SharedVisionSystem.hqUpdate();
		MapLocation target = RobotPlayer.rc.senseHQLocation();
		if(RobotPlayer.rc.sensePastrLocations(RobotPlayer.team.opponent()).length > 0) {
			target = RobotPlayer.rc.sensePastrLocations(RobotPlayer.team.opponent())[0];
		}
		RobotSwarm.SWARM_1.giveOrder(new SwarmOrder(OrderType.REGROUP, target));
		if(RobotPlayer.rc.isActive()) {
			if(Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 16).length > 0) {
				Combat.hqDefense();
			}
			else if(RobotPlayer.rc.senseRobotCount() < 25){ //TODO Check if robot count isn't needed to planned PASTRs and Noise towers
				Direction toEnemy = RobotPlayer.rc.getLocation().directionTo(RobotPlayer.rc.senseEnemyHQLocation());
				tryToSpawn(toEnemy);
			}
		}
	}
	
	private static void tryToSpawn(Direction toEnemy) {
		recurencySpawn(toEnemy, true, 8);
	}
	
	private static void recurencySpawn(Direction direction, boolean turnRight, int ticks) {
		ticks--;
		if(ticks < 0) {
			 return;
		}
		try {
			if(RobotPlayer.rc.senseObjectAtLocation(RobotPlayer.rc.getLocation().add(direction)) == null && RobotPlayer.rc.senseTerrainTile(RobotPlayer.rc.getLocation().add(direction)).isTraversableAtHeight(RobotLevel.ON_GROUND)) {
				try {
					RobotPlayer.rc.spawn(direction);
				} catch (GameActionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				recurencySpawn((turnRight ? direction.rotateRight() : direction.rotateLeft()), turnRight, ticks);
			}
		} catch (GameActionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
