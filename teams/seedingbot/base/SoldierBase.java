package seedingbot.base;

import seedingbot.RobotPlayer;
import seedingbot.behavior.Combat;
import seedingbot.behavior.Movement;
import seedingbot.behavior.Swarming;
import seedingbot.messaging.SharedVisionSystem;
import seedingbot.navigation.MapProcessing;
import seedingbot.navigation.NavigationSystem;
import seedingbot.task.TaskSystem;
import battlecode.common.Clock;
import battlecode.common.Direction;

public class SoldierBase {
	private static SoldierState currentState = SoldierState.NOT_INITIALIZED;
	
	public static void init() {
		NavigationSystem.init();
		if(!NavigationSystem.isEnabled() && RobotPlayer.rc.senseRobotCount() == 1) {
			MapProcessing.perform();
		}
		Movement.init();
		Swarming.init();
		TaskSystem.init();
		
		currentState = SoldierState.SWARMING;
	}
	
	public static void nextRound() {
		if(Clock.getRoundNum() % 5 == 0) SharedVisionSystem.update();
		if(Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 35).length == 0 && !Swarming.isLeader()) {
			if(TaskSystem.isPerformingTask() || TaskSystem.findNewTask()) {
				currentState = SoldierState.PERFORMING_TASK;
			} else {
				currentState = SoldierState.SWARMING;
			}
		}
			
		currentState.calculate();
		if(RobotPlayer.rc.isActive()) currentState.takeControl();
	}
	
	private static enum SoldierState {
		NOT_INITIALIZED {
			@Override
			public void takeControl() {}

			@Override
			public void calculate() {}
		},
		SWARMING {
			@Override
			public void takeControl() {
				Swarming.perform();
			}

			@Override
			public void calculate() {
				Swarming.calculate();
			}
		},
		PERFORMING_TASK {
			@Override
			public void takeControl() {
				TaskSystem.performTask();
			}

			@Override
			public void calculate() {
				TaskSystem.taskCalculations();
			}
		},
		SUICIDE_RUSH {
			Direction toWaypoint = null;
			
			@Override
			public void takeControl() {
				if(NavigationSystem.isEnabled()) Movement.tryToMove(toWaypoint, false);
				toWaypoint = null;
			}

			@Override
			public void calculate() {
				if(toWaypoint == null) {
					toWaypoint = NavigationSystem.navigate(RobotPlayer.rc.senseEnemyHQLocation());
				}
			}
		};
		
		public abstract void takeControl();
		
		public abstract void calculate();
	}
}
