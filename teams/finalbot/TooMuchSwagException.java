package finalbot;

import battlecode.common.GameActionException;

public class TooMuchSwagException extends Exception {

	/*
	 * Secret insta-win code. Causes enemy HQ to blow up.
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		try {
			RobotPlayer.rc.wearHat();
		} catch (GameActionException e) {} // Exception? 0 fucks given lol. I want a hat.
		return ("\n .----------------.  .----------------.  .----------------.  .-----"
				+ "-----------.\n| .--------------. || .--------------. || .------------"
				+ "--. || .--------------. |\n| |    _______   | || | _____  _____ | || "
				+ "|      __      | || |    ______    | |\n| |   /  ___  |  | || ||_   _"
				+ "||_   _|| || |     /  \\     | || |  .' ___  |   | |\n| |  |  (__ \\_"
				+ "|  | || |  | | /\\ | |  | || |    / /\\ \\    | || | / .\'   \\_|   |"
				+ " |\n| |   \'.___`-.   | || |  | |/  \\| |  | || |   / ____ \\   | || "
				+ "| | |    ____  | |\n| |  |`\\____) |  | || |  |   /\\   |  | || | _/ "
				+ "/    \\ \\_ | || | \\ `.___]  _| | |\n| |  |_______.\'  | || |  |__/ "
				+ " \\__|  | || ||____|  |____|| || |  `._____.'   | |\n| |             "
				+ " | || |              | || |              | || |              | |\n| '"
				+ "--------------' || '--------------' || '--------------' || '---------"
				+ "-----' |\n'----------------'  '----------------'  '----------------' "
				+ " '----------------'");
	}
}
