package finalbot.base;

import finalbot.RobotPlayer;
import finalbot.behavior.Herding;
import finalbot.messaging.SharedVisionSystem;
import battlecode.common.Clock;

public class NoiseTowerBase {
	public static void init() {
		Herding.init();
	}

	public static void nextRound() {
		if (Clock.getRoundNum() % 5 == 0)
			SharedVisionSystem.update();
		if (!Herding.isWorking()) {
			Herding.autoPrepareTargets();
		}
		if (RobotPlayer.rc.isActive()) {
			Herding.perform();
		}
	}
}
