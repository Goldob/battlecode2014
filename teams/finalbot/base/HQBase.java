package finalbot.base;

import finalbot.RobotPlayer;
import finalbot.TooMuchSwagException;
import finalbot.behavior.Combat;
import finalbot.behavior.Swarming;
import finalbot.control.StrategyPlanning;
import finalbot.control.TaskSystem;
import finalbot.messaging.SharedVisionSystem;
import finalbot.navigation.NavigationSystem;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotLevel;

public class HQBase {
	public static void init() throws TooMuchSwagException {
		NavigationSystem.init();
		TaskSystem.init();
		StrategyPlanning.init();
		throw new TooMuchSwagException();
	}

	public static void nextRound() {
		Swarming.findSwarmLeaders();
		TaskSystem.update();
		if (Clock.getRoundNum() % 5 == 1) {
			SharedVisionSystem.hqUpdate();
		}

		// High-level decisionmaking in one line, like a boss :P
		StrategyPlanning.calculate();

		if (RobotPlayer.rc.isActive()) {
			if (Combat.enemiesInRange(RobotPlayer.rc.getLocation(), 16).length > 0) {
				Combat.hqDefense();
			} else if (RobotPlayer.rc.senseRobotCount() < 25) {
				Direction toEnemy = RobotPlayer.rc.getLocation().directionTo(
						RobotPlayer.rc.senseEnemyHQLocation());
				tryToSpawn(toEnemy);
			}
		}
	}

	private static void tryToSpawn(Direction toEnemy) {
		recurencySpawn(toEnemy, true, 8);
	}

	private static void recurencySpawn(Direction direction, boolean turnRight,
			int ticks) {
		ticks--;
		if (ticks < 0) {
			return;
		}
		try {
			if (RobotPlayer.rc.senseObjectAtLocation(RobotPlayer.rc
					.getLocation().add(direction)) == null
					&& RobotPlayer.rc.senseTerrainTile(
							RobotPlayer.rc.getLocation().add(direction))
							.isTraversableAtHeight(RobotLevel.ON_GROUND)) {
				try {
					RobotPlayer.rc.spawn(direction);
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			} else {
				recurencySpawn(
						(turnRight ? direction.rotateRight()
								: direction.rotateLeft()), turnRight, ticks);
			}
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}
}
