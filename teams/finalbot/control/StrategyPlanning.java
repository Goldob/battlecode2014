package finalbot.control;

import finalbot.RobotPlayer;
import finalbot.behavior.Swarming.OrderType;
import finalbot.behavior.Swarming.RobotSwarm;
import finalbot.behavior.Swarming.SwarmOrder;
import finalbot.control.Task.TaskType;
import finalbot.messaging.ChannelType;
import finalbot.messaging.SharedVisionSystem;
import finalbot.messaging.SharedVisionSystem.EnemyData;
import finalbot.navigation.NavigationNode;
import finalbot.navigation.NavigationSystem;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotType;

//It's not really a strategy
public class StrategyPlanning {
	private static double totalCowGrowth;
	private static double averageCowGrowth;

	private static MapLocation bestLocation;
	private static boolean pastrScheduled;
	private static boolean ntScheduled;

	private static boolean pastrFound;
	private static boolean ntFound;

	private static int mapSize;

	private static Strategy currentStrategy;

	private StrategyPlanning() {}

	public static void init() {
		totalCowGrowth = 0.0;
		averageCowGrowth = 0.0;
		mapSize = RobotPlayer.rc.getMapHeight() * RobotPlayer.rc.getMapWidth();
		currentStrategy = Strategy.REGROUP;

		bestLocation = null;
	}

	public static void calculate() {
		checkLocation();
		if (Clock.getRoundNum() % 5 == 1) {
			if (!pastrFound
					&& (RobotPlayer.rc.senseRobotCount() < 5 || RobotPlayer.rc
							.senseRobotCount() * 1.5 < SharedVisionSystem.globalData.length)) {
				currentStrategy = Strategy.REGROUP;
			} else if (NavigationSystem.isEnabled() && growthCheck()
					&& Clock.getRoundNum() % 25 == 1) {
				if (bestLocation == null)
					findLocation();
				else {
					if (averageCowGrowth >= 1.0
							|| (averageCowGrowth >= 0.4 && mapSize >= 3600)
							&& RobotPlayer.rc
									.sensePastrLocations(RobotPlayer.team
											.opponent()).length <= 2) {
						currentStrategy = Strategy.DEF;
					} else {
						currentStrategy = Strategy.RUSH;
					}
				}
			}
		}

		if (NavigationSystem.isEnabled() && growthCheck()
				&& Clock.getRoundNum() % 25 == 1) {
			if (bestLocation == null)
				findLocation();
			else {
				if (averageCowGrowth >= 1.0
						|| (averageCowGrowth >= 0.3 && mapSize >= 3600)) {
					currentStrategy = Strategy.DEF;
				} else {
					currentStrategy = Strategy.RUSH;
				}
			}
		}
		currentStrategy.updateOrders();
	}

	private static void checkLocation() {
		pastrFound = false;
		ntFound = false;
		if (bestLocation != null) {
			for (Robot robot : RobotPlayer.rc.senseNearbyGameObjects(
					Robot.class, bestLocation, 2, RobotPlayer.team)) {
				try {
					RobotType type = RobotPlayer.rc.senseRobotInfo(robot).type;
					if (type == RobotType.NOISETOWER) {
						ntFound = true;
						ntScheduled = false;
					} else if (type == RobotType.PASTR) {
						pastrFound = true;
						pastrScheduled = false;
					}
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static void findLocation() {
		NavigationNode best9x9Node = null;
		NavigationNode best3x3Node = null;
		double bestValue = 0.0;
		for (int x = NavigationSystem.data.width / 9; --x >= 0;) {
			for (int y = NavigationSystem.data.height / 9; --y >= 0;) {
				double value = getTacticalNode(x, y).calculateHeuristicValue();
				if (value > bestValue) {
					best9x9Node = new NavigationNode(x, y);
					bestValue = value;
				}
			}
		}
		bestValue = 0.0;
		if (best9x9Node != null) {
			for (int difX = 3; --difX >= 0;) {
				for (int difY = 3; --difY >= 0;) {
					double value = get3x3Growth(best9x9Node.x * 3 + difX,
							best9x9Node.y * 3 + difY);
					if (value > bestValue) {
						best3x3Node = new NavigationNode(best9x9Node.x * 3
								+ difX, best9x9Node.y * 3 + difY);
						bestValue = value;
					}
				}
			}
			if (best3x3Node != null) {
				bestLocation = new MapLocation(best3x3Node.x * 3 + 1,
						best3x3Node.y * 3 + 1);
			}
		}
	}

	private static boolean growthCheck() {
		if (totalCowGrowth <= 0.0) {
			totalCowGrowth = ChannelType.TOTAL_COW_GROWTH.read(0);
			if (totalCowGrowth > 0.0) {
				averageCowGrowth = totalCowGrowth / mapSize;
			}
		}
		return totalCowGrowth > 0.0;
	}

	private static double get3x3Growth(int x, int y) {
		return ChannelType.COW_GROWTH_3x3NODES.read(x + 36 * y);
	}

	private static Tactical9x9Node getTacticalNode(int x, int y) {
		return Tactical9x9Node.read(ChannelType.TACTICAL_DATA_9x9NODES.read(x
				+ 12 * y));
	}

	public static enum Strategy {
		REGROUP {
			@Override
			public void updateOrders() {
				TaskSystem.cancelAllTasks();
				pastrScheduled = false;
				ntScheduled = false;
				MapLocation rallyPoint = RobotPlayer.rc.senseHQLocation();
				if(bestLocation != null) {
					rallyPoint = bestLocation;
				}
				RobotSwarm.SWARM_1.giveOrder(new SwarmOrder(OrderType.DEFEND,
						rallyPoint));
				RobotSwarm.SWARM_2.giveOrder(new SwarmOrder(OrderType.DEFEND,
						rallyPoint));
				RobotSwarm.SWARM_3.giveOrder(new SwarmOrder(OrderType.DEFEND,
						rallyPoint));
			}
		},
		RUSH {
			@Override
			public void updateOrders() {
				MapLocation enemyHqLoc = RobotPlayer.rc.senseEnemyHQLocation();
				MapLocation bestLoc = RobotPlayer.rc.senseHQLocation();
				int bestValue = Integer.MIN_VALUE;

				for (MapLocation location : RobotPlayer.rc
						.sensePastrLocations(RobotPlayer.team.opponent())) {
					int value = location.distanceSquaredTo(enemyHqLoc);
					if (value > bestValue) {
						bestValue = value;
						bestLoc = location;
					}
				}
				
				if(RobotPlayer.rc.senseRobotCount() >= 15) {
					if (!(pastrFound || pastrScheduled)) {
						TaskSystem.scheduleTask(new Task(bestLocation, 2000,
								TaskType.BUILD_PASTR));
						ntScheduled = true;
					}
					if (!(ntFound || ntScheduled)) {
						TaskSystem.scheduleTask(new Task(bestLocation, 2000,
								TaskType.BUILD_NOISETOWER));
						ntScheduled = true;
					}
					RobotSwarm.SWARM_3.giveOrder(new SwarmOrder(OrderType.DEFEND,
							bestLocation));
				} else {
					RobotSwarm.SWARM_3.giveOrder(new SwarmOrder(OrderType.ATTACK,
							bestLoc));
				}
				RobotSwarm.SWARM_1.giveOrder(new SwarmOrder(OrderType.ATTACK,
						bestLoc));
				RobotSwarm.SWARM_2.giveOrder(new SwarmOrder(OrderType.ATTACK,
						bestLoc));
			}
		},
		DEF {
			@Override
			public void updateOrders() {
				if (bestLocation != null) {
					if (!(pastrFound || pastrScheduled)) {
						TaskSystem.scheduleTask(new Task(bestLocation, 2000,
								TaskType.BUILD_PASTR));
						pastrScheduled = true;
					}
					if (!(ntFound || ntScheduled)) {
						TaskSystem.scheduleTask(new Task(bestLocation, 2000,
								TaskType.BUILD_NOISETOWER));
						ntScheduled = true;
					}
					RobotSwarm.SWARM_1.giveOrder(new SwarmOrder(
							OrderType.DEFEND, bestLocation));
					RobotSwarm.SWARM_2.giveOrder(new SwarmOrder(
							OrderType.DEFEND, bestLocation));
					RobotSwarm.SWARM_3.giveOrder(new SwarmOrder(
							OrderType.DEFEND, bestLocation));
					if (SharedVisionSystem.globalData.length != 0) {
						MapLocation closestEnemy = null;
						int closestDist = 25;
						for (EnemyData enemy : SharedVisionSystem.globalData) {
							if (bestLocation.distanceSquaredTo(enemy.location) <= closestDist) {
								closestDist = bestLocation
										.distanceSquaredTo(enemy.location);
								closestEnemy = enemy.location;
							}
						}
						if (closestEnemy != null) {
							Direction toEnemy = bestLocation
									.directionTo(closestEnemy);
							RobotSwarm.SWARM_1.giveOrder(new SwarmOrder(
									OrderType.DEFEND, bestLocation.add(
											toEnemy.rotateLeft(), 2)));
							RobotSwarm.SWARM_2.giveOrder(new SwarmOrder(
									OrderType.DEFEND, bestLocation.add(toEnemy,
											2)));
							RobotSwarm.SWARM_3.giveOrder(new SwarmOrder(
									OrderType.DEFEND, bestLocation.add(
											toEnemy.rotateRight(), 2)));
						}
					}
				}
				if(RobotPlayer.rc.senseRobotCount() >= 15) {
					MapLocation bestLoc = null;
					MapLocation enemyHqLoc = RobotPlayer.rc.senseEnemyHQLocation();
					int bestValue = Integer.MIN_VALUE;
					for (MapLocation location : RobotPlayer.rc
							.sensePastrLocations(RobotPlayer.team.opponent())) {
						int value = location.distanceSquaredTo(enemyHqLoc);
						if (value > bestValue) {
							bestValue = value;
							bestLoc = location;
						}
					}
					if(bestLoc != null) {
						RobotSwarm.SWARM_3.giveOrder(new SwarmOrder(OrderType.ATTACK,
								bestLoc));
					}
				}
			}
		};
		public abstract void updateOrders();
	}
}