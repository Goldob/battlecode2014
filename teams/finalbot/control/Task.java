package finalbot.control;

import finalbot.RobotPlayer;
import finalbot.behavior.Combat;
import finalbot.behavior.Movement;
import finalbot.navigation.NavigationSystem;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;

public class Task {
	private MapLocation destination;
	private int expiryRound;
	private TaskType type;

	private int roundTaken;

	private Direction toWaypoint = null;

	public Task(MapLocation destination, int expiryRound, TaskType type) {
		this.destination = destination;
		this.expiryRound = expiryRound;
		this.type = type;
		this.roundTaken = Clock.getRoundNum();
	}

	public void combat() {
		if (toWaypoint != null) {
			Combat.killEnemies(toWaypoint, false);
		} else {
			Combat.killEnemies(
					RobotPlayer.rc.getLocation().directionTo(destination),
					false);
		}
	}

	public void calculate() {
		if (RobotPlayer.rc.getLocation().distanceSquaredTo(destination) > 2
				&& toWaypoint == null) {
			toWaypoint = NavigationSystem.navigate(destination);
		}
	}

	public void perform() {
		if (RobotPlayer.rc.getLocation().distanceSquaredTo(destination) <= 2) {
			type.perform();
		} else {
			if (toWaypoint != null) {
				Movement.tryToMove(toWaypoint, false);
			}
			toWaypoint = null;
		}
	}

	public boolean isExpired() {
		return Clock.getRoundNum() >= expiryRound
				|| Clock.getRoundNum() >= roundTaken + 125;
	}

	public int toMessage() {
		return ((type.ordinal() & 0x000000FF) << 24)
				| ((expiryRound / 10 & 0x000000FF) << 16)
				| ((destination.x & 0x000000FF) << 8) | destination.y
				& 0x000000FF;
	}

	public static Task read(int message) {
		int ordinal = (message >> 24) & 0x000000FF;
		int expiryRound = 10 * ((message >> 16) & 0x000000FF);
		int x = (message >> 8) & 0x000000FF;
		int y = message & 0x000000FF;
		return new Task(new MapLocation(x, y), expiryRound,
				TaskType.values()[ordinal]);
	}

	public static enum TaskType {
		BUILD_PASTR {
			@Override
			public void perform() {
				try {
					RobotPlayer.rc.construct(RobotType.PASTR);
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		},
		BUILD_NOISETOWER {
			@Override
			public void perform() {
				try {
					RobotPlayer.rc.construct(RobotType.NOISETOWER);
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		};

		public abstract void perform();
	}
}
