package finalbot.control;

public class Tactical9x9Node {
	public double cowGrowth;
	public int distanceToHQ;

	public Tactical9x9Node(double cowGrowth, int distanceToHQ) {
		this.cowGrowth = cowGrowth;
		this.distanceToHQ = distanceToHQ;
	}

	public Tactical9x9Node() {
		cowGrowth = 0.0;
		distanceToHQ = Integer.MAX_VALUE;
	}

	public double calculateHeuristicValue() {
		return cowGrowth / Math.pow(distanceToHQ, 1.5);
	}

	public double getAverageCowGrowth() {
		return cowGrowth / 81;
	}

	public static Tactical9x9Node read(int message) {
		int cowGrowth = (message >> 16) & 0x0000FFFF;
		int distanceToHQ = (message) & 0x0000FFFF;
		return new Tactical9x9Node(cowGrowth, distanceToHQ);
	}

	public int write() {
		return ((((int) cowGrowth) & 0x0000FFFF) << 16)
				| (distanceToHQ & 0x0000FFFF);
	}
}
