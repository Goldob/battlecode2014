package finalbot.navigation;

import java.util.LinkedList;
import java.util.Queue;

import finalbot.RobotPlayer;
import finalbot.messaging.ChannelType;



import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public class NavigationSystem {
	private static boolean enabled;
	public static NavigationData data;

	private static Queue<MapLocation> microPath;
	private static Queue<NavigationNode> macroPath;

	private static int roundsWaiting;
	private static final int PATIENCE = 15;

	private static MapLocation location;
	private static MapLocation macroDestination;
	private static MapLocation microDestination;

	private NavigationSystem() {
	}

	public static void init() {
		data = new NavigationData(-1, -1);
		tryToEnable();
		macroDestination = null;
		microDestination = null;
		roundsWaiting = 0;
	}

	public static Queue<MapLocation> microPathing(MapLocation from,
			MapLocation destination, boolean limitedArea) {
		int fromX = from.x / 3;
		int fromY = from.y / 3;
		int destX = destination.x / 3;
		int destY = destination.y / 3;
		Direction previousNode[][] = new Direction[34][34];
		Queue<MapLocation> calculatedPath = new LinkedList<MapLocation>();
		Queue<NavigationNode> bfsQueue = new LinkedList<NavigationNode>();
		bfsQueue.add(new NavigationNode(destX, destY));
		previousNode[destX][destY] = Direction.NONE;
		while (!bfsQueue.isEmpty()) {
			NavigationNode consideredNode = bfsQueue.poll();
			consideredNode.read(data.read3x3NodeData(consideredNode.x,
					consideredNode.y));
			if (consideredNode.x == fromX && consideredNode.y == fromY) {
				int x = consideredNode.x;
				int y = consideredNode.y;
				while (true) {
					calculatedPath.add(new MapLocation(x * 3 + 1, y * 3 + 1));
					Direction toPrev = previousNode[x][y];
					if (toPrev == Direction.NONE) {
						break;
					}
					x += toPrev.dx;
					y += toPrev.dy;
				}
				break;
			}
			for (int i = 8; --i >= 0;) {
				Direction dir = Direction.values()[i];
				if (consideredNode.isConnected(dir)) {
					int x = consideredNode.x + dir.dx;
					int y = consideredNode.y + dir.dy;
					if (previousNode[x][y] == null) {
						if (limitedArea
								&& !((x >= fromX - 1 && y >= fromY - 1
										&& x <= fromX + 1 && y <= fromY + 1) || (x >= destX - 1
										&& y >= destY - 1 && x <= destX + 1 && y <= destY + 1)))
							continue;
						previousNode[x][y] = dir.opposite();
						bfsQueue.add(new NavigationNode(x, y));
					}
				}
			}
		}
		return calculatedPath;
	}

	public static Queue<NavigationNode> macroPathing(MapLocation from,
			MapLocation destination) {
		int fromX = from.x / 9;
		int fromY = from.y / 9;
		int destX = destination.x / 9;
		int destY = destination.y / 9;
		Direction previousNode[][] = new Direction[12][12];
		Queue<NavigationNode> calculatedPath = new LinkedList<NavigationNode>();
		Queue<NavigationNode> bfsQueue = new LinkedList<NavigationNode>();
		bfsQueue.add(new NavigationNode(destX, destY));
		previousNode[destX][destY] = Direction.NONE;
		while (!bfsQueue.isEmpty()) {
			NavigationNode consideredNode = bfsQueue.poll();
			consideredNode.read(data.read9x9NodeData(consideredNode.x,
					consideredNode.y));
			if (consideredNode.x == fromX && consideredNode.y == fromY) {
				int x = consideredNode.x;
				int y = consideredNode.y;
				while (true) {
					calculatedPath.add(new NavigationNode(x, y));
					Direction toPrev = previousNode[x][y];
					if (toPrev == Direction.NONE) {
						break;
					}
					x += toPrev.dx;
					y += toPrev.dy;
				}
				break;
			}
			for (int i = 8; --i >= 0;) {
				Direction dir = Direction.values()[i];
				if (consideredNode.isConnected(dir)) {
					int x = consideredNode.x + dir.dx;
					int y = consideredNode.y + dir.dy;
					if (previousNode[x][y] == null) {
						previousNode[x][y] = dir.opposite();
						bfsQueue.add(new NavigationNode(x, y));
					}
				}
			}
		}
		return calculatedPath;
	}

	public static Direction navigate(MapLocation destination) {
		if (!isEnabled()) {
			return RobotPlayer.rc.getLocation().directionTo(destination);
		}
		location = RobotPlayer.rc.getLocation();
		MapLocation nextStep = macroNavigation(destination);
		return microNavigation(nextStep);
	}

	//Sometimes it wants to go where it just can't... wut da fuk
	public static MapLocation macroNavigation(MapLocation destination) {
		if (!areEqual(destination, macroDestination)) {
			macroDestination = destination;
			macroPath = macroPathing(location, destination);
		}
		if (macroPath.isEmpty()) {
			return destination;
		}
		NavigationNode node = macroPath.peek();
		MapLocation nextStep = new MapLocation(node.x * 9 + 4, node.y * 9 + 4);
		if (location.distanceSquaredTo(nextStep) > 400) {
			macroPath = macroPathing(location, destination);
			return macroNavigation(destination);
		}
		try {
			if (areEqual(location, nextStep)
					|| (location.distanceSquaredTo(nextStep) <= 2 && RobotPlayer.rc
							.senseObjectAtLocation(nextStep) != null)) {
				macroPath.remove();
				return macroNavigation(destination);
			}
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		return nextStep;
	}

	public static Direction microNavigation(MapLocation destination) {
		if (!areEqual(destination, microDestination)) {
			microDestination = destination;
			microPath = microPathing(location, destination,
					!areEqual(macroDestination, microDestination));
			roundsWaiting = 0;
		}
		if (microPath.isEmpty()) {
			if (location.distanceSquaredTo(destination) <= 2) {
				return location.directionTo(destination);
			}
			roundsWaiting++;
			if (roundsWaiting > PATIENCE) {
				microPath = microPathing(location, destination,
						!areEqual(macroDestination, microDestination));
				roundsWaiting = 0;
				return microNavigation(destination);
			} else {
				return location.directionTo(destination);
			}
		}
		MapLocation nextStep = microPath.peek();
		if (location.distanceSquaredTo(nextStep) > 72) {
			microPath = microPathing(location, destination, false);
			roundsWaiting = 0;
			return microNavigation(destination);
		}
		try {
			if (areEqual(location, nextStep)
					|| (location.distanceSquaredTo(nextStep) <= 2 && RobotPlayer.rc
							.senseObjectAtLocation(nextStep) != null)) {
				microPath.remove();
				roundsWaiting = 0;
				return microNavigation(destination);
			}
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		roundsWaiting++;
		return location.directionTo(nextStep);
	}

	public static boolean isEnabled() {
		if (!enabled) {
			tryToEnable();
		}
		return enabled;
	}

	private static void tryToEnable() {
		int message = ChannelType.NAVIGATION_DATA.read(0);
		if (message != 0) {
			data.read(message);
			enabled = true;
		}
	}

	public static NavigationNode get3x3Node(int x, int y) {
		NavigationNode node = new NavigationNode(x, y);
		node.read(data.read3x3NodeData(x, y));
		return node;
	}

	public static NavigationNode get9x9Node(int x, int y) {
		NavigationNode node = new NavigationNode(x, y);
		node.read(data.read9x9NodeData(x, y));
		return node;
	}

	public static boolean areEqual(MapLocation loc1, MapLocation loc2) {
		return (loc1 != null && loc2 != null && loc1.x == loc2.x && loc1.y == loc2.y);
	}
}