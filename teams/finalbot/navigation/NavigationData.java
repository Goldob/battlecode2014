package finalbot.navigation;

import finalbot.messaging.ChannelType;

public class NavigationData {
	public int width;
	public int height;

	public NavigationData(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public void read(int message) {
		width = (message & 0x0000FFFF);
		height = ((message >> 16) & 0x0000FFFF);
	}

	public int write() {
		int savedInt = width;
		savedInt = savedInt | (height << 16);
		return savedInt;
	}

	public int read3x3NodeData(int x, int y) {
		return ChannelType.NAVIGATION_GRAPH_3x3NODES.read(x + 36 * y);
	}

	public int read9x9NodeData(int x, int y) {
		return ChannelType.NAVIGATION_GRAPH_9x9NODES.read(x + 12 * y);
	}
}
