package finalbot.behavior;

import java.util.LinkedList;
import java.util.List;

import finalbot.RobotPlayer;
import finalbot.behavior.Movement.DirectionValidator;



import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.Robot;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TerrainTile;

public class Combat {
	public static void killEnemies(Direction direction, boolean inAttack) {
		MapLocation location = RobotPlayer.rc.getLocation();
		// Some PASTR denying here, rarely works
		for (MapLocation pastrLocation : RobotPlayer.rc
				.sensePastrLocations(RobotPlayer.team)) {
			if (location.distanceSquaredTo(pastrLocation) <= 10) {
				try {
					if (RobotPlayer.rc.senseRobotInfo((Robot) RobotPlayer.rc
							.senseObjectAtLocation(pastrLocation)).health <= 10.0) {
						RobotPlayer.rc.attackSquare(pastrLocation);
						return;
					}
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
		// Kill or run? Maybe kill cows? To be or not to be?
		Robot[] enemiesInRange = enemiesInRange(location, 10);
		if (enemiesInRange.length > 0) {
			int hitCount = 0;
			boolean killInOneShot = false;
			boolean killInTwoShots = false;
			for (Robot enemy : enemiesInRange) {
				try {
					RobotInfo info = RobotPlayer.rc.senseRobotInfo(enemy);
					if (info.type == RobotType.SOLDIER) {
						hitCount++;
						if (info.health <= 20.0) {
							killInTwoShots = true;
							if (info.health <= 10.0) {
								killInOneShot = true;
							}
						}
					}
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}

			if ((killInOneShot ? hitCount - 1 : hitCount) * 10.0 >= RobotPlayer.rc
					.getHealth()) {
				retreat(direction);
				return;
			} else if ((killInOneShot ? hitCount - 1
					: (killInTwoShots ? hitCount - 0.5 : hitCount)) * 20.0 >= RobotPlayer.rc
					.getHealth()) {
				Robot[] enemiesInSmallRange = enemiesInRange(location, 5);
				if (RobotPlayer.rc.senseTerrainTile(location) == TerrainTile.ROAD
						|| enemiesInSmallRange.length > 0) {
					try {
						if (enemiesInSmallRange.length > 0) {
							yolo(RobotPlayer.rc
									.senseRobotInfo(enemiesInSmallRange[0]).location);
						} else {
							yolo(RobotPlayer.rc
									.senseRobotInfo(enemiesInRange[0]).location);
						}
						return;
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				}
			}
			pickTarget();
		} else {
			if (!Movement.tryToMove(direction, false, new int[] { 0, -1, 1 },
					new BeCareful())) {
				if (!(inAttack && disturbHerding())) {
					Movement.tryToMove(direction, false, new BeCareful());
				}
			}
		}
	}

	private static boolean disturbHerding() {
		MapLocation myLocation = RobotPlayer.rc.getLocation();
		MapLocation bestTarget = null;
		for (MapLocation pastrLocation : RobotPlayer.rc
				.sensePastrLocations(RobotPlayer.team.opponent())) {
			if (pastrLocation.distanceSquaredTo(myLocation) <= 35) {
				for (MapLocation nearbyLocation : MapLocation
						.getAllMapLocationsWithinRadiusSq(pastrLocation, 5)) {
					try {
						if (myLocation.distanceSquaredTo(nearbyLocation) <= 10
								&& RobotPlayer.rc
										.senseCowsAtLocation(nearbyLocation) >= 450) {
							bestTarget = nearbyLocation;
						}
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				}
			}
		}
		if (bestTarget != null) {
			try {
				RobotPlayer.rc.attackSquare(bestTarget);
				return true;
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public static void retreat(Direction preferable) {
		int minCount = Integer.MAX_VALUE;
		List<Direction> possibleDirections = new LinkedList<Direction>();
		for (int i = 8; --i >= 0;) {
			Direction dir = Direction.values()[i];
			int enemies = enemiesInRange(RobotPlayer.rc.getLocation().add(dir),
					10).length;
			if (enemies < minCount && Movement.isValid(dir)) {
				minCount = enemies;
				possibleDirections = new LinkedList<Direction>();
			}
			if (enemies == minCount && Movement.isValid(dir)) {
				possibleDirections.add(dir);
			}
		}
		int minDifference = Integer.MAX_VALUE;
		Direction bestDirection = Direction.NONE;
		for (Direction dir : possibleDirections) {
			int difference = Math.abs(preferable.ordinal() - dir.ordinal());
			if (difference <= minDifference) {
				bestDirection = dir;
				minDifference = difference;
			}
		}
		try {
			RobotPlayer.rc.move(bestDirection);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

	private static void pickTarget() {
		MapLocation best = null;
		double bestValue = 0.0;
		for (Robot enemy : enemiesInRange(RobotPlayer.rc.getLocation(), 10)) {
			double consideredValue = calculateTargetValue(enemy);
			if (consideredValue > bestValue) {
				try {
					best = RobotPlayer.rc.senseLocationOf(enemy);
					bestValue = consideredValue;
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
		if (best != null) {
			try {
				RobotPlayer.rc.attackSquare(best);
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
	}

	public static void yolo(MapLocation target) {
		for (int i = 5; --i >= 0;) {
			if (enemiesInRange(RobotPlayer.rc.getLocation(), 2).length > alliesInRange(
					RobotPlayer.rc.getLocation(), 2).length) {
				try {
					RobotPlayer.rc.selfDestruct();
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			} else if (RobotPlayer.rc.isActive()) {
				Movement.tryToMove(
						RobotPlayer.rc.getLocation().directionTo(target), false);
			}
			RobotPlayer.rc.yield();
		}
	}

	private static double calculateTargetValue(Robot target) {
		RobotInfo targetData = null;
		try {
			targetData = RobotPlayer.rc.senseRobotInfo(target);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		if (targetData.type == RobotType.NOISETOWER) {
			return 1.0 + (100 - targetData.health) / 100;
		} else if (targetData.type == RobotType.SOLDIER) {
			return 5.0 + (100 - targetData.health) / 100;
		} else if (targetData.type == RobotType.PASTR) {
			return 4.0 + ((200 - targetData.health) / 100);
		}
		return 0.0;
	}

	public static Robot[] enemiesInRange(MapLocation location, int rangeSquared) {
		return RobotPlayer.rc.senseNearbyGameObjects(Robot.class, location,
				rangeSquared, RobotPlayer.rc.getTeam().opponent());
	}

	public static Robot[] alliesInRange(MapLocation location, int rangeSquared) {
		return RobotPlayer.rc.senseNearbyGameObjects(Robot.class, location,
				rangeSquared, RobotPlayer.rc.getTeam());
	}

	public static boolean inRangeOfEnemyHQ(MapLocation location) {
		return location
				.distanceSquaredTo(RobotPlayer.rc.senseEnemyHQLocation()) <= 32;
	}

	public static class BeCareful implements DirectionValidator {

		/*
		 * Returns true, if: a) Doesn't close combat distance to any enemy
		 * soldier b) Closes combat distance only to enemy soldiers already in
		 * fight c) Closes combat distance, but we heavily outnumber them and
		 * the robot has full HP AND we don't go into HQ range
		 */
		@Override
		public boolean isValid(Direction direction) {
			MapLocation location = RobotPlayer.rc.getLocation();
			Robot[] enemiesInRange = enemiesInRange(location.add(direction), 10);
			if (inRangeOfEnemyHQ(location.add(direction))) {
				return false;
			}
			if (enemiesInRange.length == 0) {
				return true;
			} else {
				// int soldierCount = 0;
				int notInCombat = 0;
				for (Robot enemy : enemiesInRange) {
					try {
						if (RobotPlayer.rc.senseRobotInfo(enemy).type == RobotType.SOLDIER) {
							// soldierCount++;
							if (alliesInRange(
									RobotPlayer.rc.senseLocationOf(enemy), 10).length == 0) {
								notInCombat++;
							}
						}
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				}
				/*
				 * if(notInCombat*30.0 >= RobotPlayer.rc.getHealth() ||
				 * soldierCount*10.0 >= RobotPlayer.rc.getHealth()) { return
				 * false; } else
				 */if (notInCombat == 0) {
					return true;
				} /*
				 * else if(RobotPlayer.rc.senseTerrainTile(location) ==
				 * TerrainTile.ROAD) { return
				 * alliesInRange(location.add(direction, 2), 15).length+1 >
				 * enemiesInRange.length; } else { return
				 * alliesInRange(location.add(direction, 2), 15).length+1 >
				 * 2*enemiesInRange.length; }
				 */
				return false;
			}
		}
	}

	public static void hqDefense() {
		MapLocation best = null;
		int bestValue = 0;
		for (MapLocation possibleTarget : MapLocation
				.getAllMapLocationsWithinRadiusSq(RobotPlayer.rc.getLocation(),
						15)) {
			int newValue = (RobotPlayer.rc.senseNearbyGameObjects(Robot.class,
					possibleTarget, 2, RobotPlayer.team.opponent()).length);
			newValue -= (RobotPlayer.rc.senseNearbyGameObjects(Robot.class,
					possibleTarget, 2, RobotPlayer.team).length);
			try {
				Robot robot = (Robot) RobotPlayer.rc
						.senseObjectAtLocation(possibleTarget);
				if (robot != null) {
					newValue = newValue
							+ (robot.getTeam() == RobotPlayer.rc.getTeam() ? -1
									: 1);
				}
			} catch (GameActionException e) {
				e.printStackTrace();
			}
			if (newValue > bestValue) {
				bestValue = newValue;
				best = possibleTarget;
			}
		}
		if (best != null) {
			try {
				RobotPlayer.rc.attackSquare(best);
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
	}
}