package finalbot.behavior;

import finalbot.RobotPlayer;
import battlecode.common.Direction;
import battlecode.common.GameActionException;

public class Movement {
	private static int directionalLooks[];

	public static void init() {
		if (RobotPlayer.rc.getRobot().getID() % 2 == 1) {
			directionalLooks = new int[] { 0, 1, -1, 2, -2};
		} else {
			directionalLooks = new int[] { 0, -1, 1, -2, 2};
		}
	}

	public static boolean tryToMove(Direction direction, boolean isSneaking,
			DirectionValidator... validators) {
		return tryToMove(direction, isSneaking, Movement.directionalLooks,
				validators);
	}

	public static boolean tryToMove(Direction direction, boolean isSneaking,
			int[] directionalLooks, DirectionValidator... validators) {
		int forwardInt = direction.ordinal();
		for (int directionalOffset : directionalLooks) {
			Direction trialDir = Direction.values()[(forwardInt
					+ directionalOffset + 8) % 8];
			if (isValid(trialDir, validators)) {
				try {
					if (isSneaking)
						RobotPlayer.rc.sneak(trialDir);
					else
						RobotPlayer.rc.move(trialDir);
				} catch (GameActionException e) {
					e.printStackTrace();
				}
				return true;
			}
		}
		return false;
	}

	public static boolean isValid(Direction direction,
			DirectionValidator... validators) {
		if (!RobotPlayer.rc.canMove(direction))
			return false;
		for (DirectionValidator validator : validators) {
			if (!validator.isValid(direction)) {
				return false;
			}
		}
		return true;
	}

	public static interface DirectionValidator {
		public boolean isValid(Direction direction);
	}
}
